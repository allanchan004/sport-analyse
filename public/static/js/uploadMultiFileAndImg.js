//上传多文件和图片
//var fileArr = [];
layui.use('upload', function(){
    var $ = layui.jquery;
    var upload = layui.upload;
    var size = 1024*10;
    //var file ='';

    var uploadKey = [],uploadConfig = null;
    upload.render({
        elem: '#file-slide-pc',
        url: '/finances/upload/index',
        size: size,
        //auto: true,
        auto: false,
        accept: 'file',
        exts: 'pdf|jpg|png|jpeg',
        multiple: true,

        choose: function(obj){
            var flag = true;
            obj.preview(function (index, file, result) {
                var fileName = file.name; //针对一个文件直接赋值就可以了
                var ext = fileName.substr(fileName.lastIndexOf('.') + 1);//获得后缀

                var size = file.size;
                if ($.inArray(ext, ["jpg", "png", "jpeg"]) >= 0) {
                    if (size > 1024*1024*6) {
                        layer.msg("图片文件不能超过6M！");

                        //setTimeout(function () {
        //                    console.log(1111);
        //                    layer.msg("图片文件不能超过6M-1111！");
                            //关闭当前窗口
                            //closeIframe();
                        //}, 3000);

                        flag = false;
                        return false;
                    }
                }

                if ($.inArray(ext, ["pdf"]) >= 0) {
                    if (size > 1024*1024*10) {
                        layer.msg("非图片文件不能超过10M！");

                        //setTimeout(function () {
        //                    console.log(22222);
        //                    layer.msg("非图片文件不能超过10M-22222！");
                            //关闭当前窗口
                            //closeIframe();
                        //}, 3000);

                        flag = false;
                        return false;
                    }
                }

                obj.upload(index, file);
                return flag;
                });
        },

        before: function(obj) {
            var uploadDir=$('#uploadDir').val();
            this.data = {
                name: uploadDir
            };
            var files = this.files = obj.pushFile();
            var fileName;
            var exts_str = "pdf,jpg,png,jpeg";
            for(var key in files){
                fileName = files[key].name; //针对一个文件直接赋值就可以了
                var ext = fileName.substr(fileName.lastIndexOf('.') + 1);//获得后缀
                if(exts_str.indexOf(ext) == -1){
                    layer.msg("请上传文件，或更改资源类型！");
                    return false;
                }
            }
            layer.msg('文件上传中...', {
                icon: 16,
                shade: 0.01,
                time: 0
            })
        },
        done: function(res,index) {
            //console.log(res);
            layer.close(layer.msg());   //关闭上传提示窗口
            //uploadConfig = res;


            //var res = uploadConfig;
            for(var key in this.files) {
                if( $.inArray(key,uploadKey) !== -1 ) continue;
                uploadKey.push(key);

                var fileName = this.files[key].name;
                var ext = fileName.substr(fileName.lastIndexOf('.') + 1);//获得后缀
                var upload_dir_name = res.data.dir + random_string(10) + '.' + ext;
                var request = new FormData();

                request.append("OSSAccessKeyId", res.data.accessid);//Bucket 拥有者的Access Key Id。
                request.append("policy", res.data.policy);//policy规定了请求的表单域的合法性
                request.append("Signature", res.data.signature);//根据Access Key Secret和policy计算的签名信息，OSS验证该签名信息从而验证该Post请求的合法性
                //---以上都是阿里的认证策略
                request.append("key", upload_dir_name);//文件名字，可设置路径
                request.append("success_action_status", '200');// 让服务端返回200,不然，默认会返回204
                // request.append('file', result);//需要上传的文件 file
                request.append('file', this.files[key]);
                request.append("callback", 'oss-cn-hangzhou.aliyuncs.com');//回调，非必选，可以在policy中自定义

                $.ajax({
                    size: size,
                    type: 'post',
                    url: res.data.host,  //上传阿里地址
                    data: request,
                    processData: false,//默认true，设置为 false，不需要进行序列化处理
                    cache: false,//设置为false将不会从浏览器缓存中加载请求信息
                    async: false,//发送同步请求
                    contentType: false,//避免服务器不能正常解析文件---------具体的可以查下这些参数的含义
                    dataType: 'text',//不涉及跨域  写json即可
                    success: function (callbackHost, request) { //callbackHost：success,request中就是 回调的一些信息，包括状态码什么的
                        console.log(request);
                        if (request != 'success') {
                            layer.msg("上传文件出错");
                        }

                        if ($.inArray(ext, ["jpg", "png", "jpeg"]) >= 0) {
                            $('#file-slide-pc-priview').append('<li class="item_img"><div class="operate"><i class="toleft layui-icon">' +
                                '</i><i class="toright layui-icon"></i><i  class="close layui-icon"></i></div>' +
                                '<img src=' + res.data.host+upload_dir_name+' class="img" ><input type="hidden" name="files1[]" value="'+ res.data.host+upload_dir_name + '" /></li>');
                        }

                        if ($.inArray(ext, ["pdf"]) >= 0) {
                            $('#file-slide-pc-priview').append('<li class="item_img"><div class="operate">' +
                                '<i class="closeFile layui-icon"></i></div>' +
                                '<a target="_blank" href="' + res.data.host + upload_dir_name + '"><img src="/static/images/u2669.png"></a><input type="hidden" name="files1[]" value="' + res.data.host + upload_dir_name + '" /></li>');
                        }
                    },
                    error: function (returndata) {
                        layer.msg("上传文件出错" + returndata.data);
                    }
                });
            }
        },
        // allDone: function(obj) {
        //     var res = uploadConfig;
        //     for(var key in this.files) {
        //         if( $.inArray(key,uploadKey) !== -1 ) continue;
        //         uploadKey.push(key);
        //
        //         var fileName = this.files[key].name;
        //         var ext = fileName.substr(fileName.lastIndexOf('.') + 1);//获得后缀
        //         var upload_dir_name = res.data.dir + random_string(10) + '.' + ext;
        //         var request = new FormData();
        //
        //         request.append("OSSAccessKeyId", res.data.accessid);//Bucket 拥有者的Access Key Id。
        //         request.append("policy", res.data.policy);//policy规定了请求的表单域的合法性
        //         request.append("Signature", res.data.signature);//根据Access Key Secret和policy计算的签名信息，OSS验证该签名信息从而验证该Post请求的合法性
        //         //---以上都是阿里的认证策略
        //         request.append("key", upload_dir_name);//文件名字，可设置路径
        //         request.append("success_action_status", '200');// 让服务端返回200,不然，默认会返回204
        //         // request.append('file', result);//需要上传的文件 file
        //         request.append('file', this.files[key]);
        //         request.append("callback", 'oss-cn-hangzhou.aliyuncs.com');//回调，非必选，可以在policy中自定义
        //
        //         $.ajax({
        //             size: size,
        //             type: 'post',
        //             url: res.data.host,  //上传阿里地址
        //             data: request,
        //             processData: false,//默认true，设置为 false，不需要进行序列化处理
        //             cache: false,//设置为false将不会从浏览器缓存中加载请求信息
        //             async: false,//发送同步请求
        //             contentType: false,//避免服务器不能正常解析文件---------具体的可以查下这些参数的含义
        //             dataType: 'text',//不涉及跨域  写json即可
        //             success: function (callbackHost, request) { //callbackHost：success,request中就是 回调的一些信息，包括状态码什么的
        //                 if (request != 'success') {
        //                     layer.msg("上传文件出错");
        //                 }
        //
        //                 //$('#file-slide-pc-priview').append('<li class="item_img"><div class="operate">' +
        //                 //    '<i class="closeFile layui-icon"></i></div>' +
        //                 //    '<a target="_blank" href="' + res.data.host + upload_dir_name + '"><img src="/static/images/u2669.png"></a><input type="hidden" name="files1[]" value="' + res.data.host + upload_dir_name + '" /></li>');

        //                 //var ext = upload_dir_name.substr(upload_dir_name.lastIndexOf('.') + 1);//获得后缀
        //                 if ($.inArray(ext, ["jpg", "png", "jpeg"]) >= 0) {
        //                     //console.log('image==='+ext);
        //                     $('#file-slide-pc-priview').append('<li class="item_img"><div class="operate"><i class="toleft layui-icon">' +
        //                         '</i><i class="toright layui-icon"></i><i  class="close layui-icon"></i></div>' +
        //                         '<img src=' + res.data.host+upload_dir_name+' class="img" ><input type="hidden" name="files1[]" value="'+ res.data.host+upload_dir_name + '" /></li>');
        //                 }
        //
        //                 if ($.inArray(ext, ["pdf"]) >= 0) {
        //                     //console.log('file==='+ext);
        //                     $('#file-slide-pc-priview').append('<li class="item_img"><div class="operate">' +
        //                         '<i class="closeFile layui-icon"></i></div>' +
        //                         '<a target="_blank" href="' + res.data.host + upload_dir_name + '"><img src="/static/images/u2669.png"></a><input type="hidden" name="files1[]" value="' + res.data.host + upload_dir_name + '" /></li>');
        //                 }
        //             },
        //             error: function (returndata) {
        //                 layer.msg("上传文件出错" + returndata.data);
        //             }
        //         });
        //     }
        // }
    });
});

//点击多图上传的X,删除当前的图片
$("body").on("click",".closeFile",function(){
    $(this).closest("li").remove();
});