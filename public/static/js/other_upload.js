layui.use('upload', function(){
    var $ = layui.jquery;
    var upload = layui.upload;
    var file ='';
    var size =1024*6;
    upload.render({
        elem: '#slide-pc-put',
        url: '/finances/upload/index',
        size: size,
        auto: true,
        exts: 'jpg|png|jpeg',
        multiple: true,
        before: function(obj) {
            var uploadDir=$('#uploadDir').val();
            this.data = {
                name: uploadDir
            }
            var files = this.files = obj.pushFile();
            var fileName;
            var i = 0;
            var j = 0;
            for(var key in files){
                i++;
                fileName = files[key].name; //针对一个文件直接赋值就可以了
            }
            //判断队列每次只保存最后一个文件
            if(i>1){
                for(var key in files){
                    j++;
                    if(i == j){
                        fileName = files[key].name;
                    }else{
                        delete files[key]; //删除队列中的文件
                    }
                }
            }
            var ext = fileName.substr(fileName.lastIndexOf('.') + 1);//获得后缀
            if("jpg,jpeg,png".indexOf(ext) == -1){
                layer.msg("请上传图片，或更改资源类型！");
                return false;
            }
            layer.msg('图片上传中...', {
                icon: 16,
                shade: 0.01,
                time: 0
            })
        },

        done: function(res,index) {
            layer.close(layer.msg());//关闭上传提示窗口
            var fileName =this.files[index].name;
            var ext = fileName.substr(fileName.lastIndexOf('.') + 1);//获得后缀
            var upload_dir_name=res.data.dir+random_string(10)+'.'+ext;
            var request = new FormData();
            request.append("OSSAccessKeyId",res.data.accessid);//Bucket 拥有者的Access Key Id。
            request.append("policy",res.data.policy);//policy规定了请求的表单域的合法性
            request.append("Signature",res.data.signature);//根据Access Key Secret和policy计算的签名信息，OSS验证该签名信息从而验证该Post请求的合法性
            //---以上都是阿里的认证策略
            request.append("key",upload_dir_name);//文件名字，可设置路径
            request.append("success_action_status",'200');// 让服务端返回200,不然，默认会返回204
            // request.append('file', result);//需要上传的文件 file
            request.append('file',this.files[index]);
            request.append("callback",'oss-cn-hangzhou.aliyuncs.com');//回调，非必选，可以在policy中自定义
            $.ajax({
                size: size,
                type:'post',
                url : res.data.host,  //上传阿里地址
                data : request,
                processData: false,//默认true，设置为 false，不需要进行序列化处理
                cache: false,//设置为false将不会从浏览器缓存中加载请求信息
                async: false,//发送同步请求
                contentType: false,//避免服务器不能正常解析文件---------具体的可以查下这些参数的含义
                dataType: 'text',//不涉及跨域  写json即可
                success : function(callbackHost, request) { //callbackHost：success,request中就是 回调的一些信息，包括状态码什么的
                    if(request !='success'){
                        layer.msg("上传图片出错");
                    }
                    $('#slide-pc-priview-put').append('<li class="item_img"><div class="operate"><i class="toleft layui-icon">' +
                        '</i><i class="toright layui-icon"></i><i  class="close layui-icon"></i></div>' +
                        '<img src=' + res.data.host+upload_dir_name+' class="img" ><input type="hidden" name="payment_img[]" value="'+ res.data.host+upload_dir_name + '" /></li>');

                },
                error : function(returndata) {
                    layer.msg("上传图片出错"+returndata.data);
                }
            });
        }
    });
});