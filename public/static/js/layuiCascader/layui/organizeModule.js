$(document).ready(function(){
	if($(".organizeModule").length>0){
		getdata();
		// 组织架构调用
		$(".organizeModule").each(function() {
			var id=$(this).find(".layui-input").attr("id");
			var lastVal = $(this).find(".lastLeveVal").val();
			if(lastVal==""){
				organize("#"+id);
			}else{
				$.ajax({
					url:'/finances/oa/organization/cascade?organic_id='+lastVal,
					type:'get',
					success:function(res){
						var pidArr = res.pid_arr.slice(1);
						organize("#"+id,pidArr);
					}
				});
			}
		});
	}
})
function getdata(){
	$.ajax({
		url:'/finances/oa/simpleTree/list',
		type:'get',
		async: false,
		success:function(res){
			var data = res.data.tree;
			treeData = deepCopy(data);
			treeData = JSON.parse(JSON.stringify(treeData).replace(/name/g, 'label'));
			treeData = JSON.parse(JSON.stringify(treeData).replace(/id/g, 'value'));
			treeData = JSON.parse(JSON.stringify(treeData).replace(/pvalue/g, 'pid'));
			return treeData;
		},
		error:function(err){
			console.log(err);
		}
	});
}

function organize(id,e){
	layui.use(['form', "jquery", "cascader", "form"], function() {
		var $ = layui.jquery;
		var cascader = layui.cascader;

		return cascader({
			elem: id,
			data: treeData,
			// triggerType: "change",
			// showLastLevels: true,
			changeOnSelect: true,
			value: e?e:[],
			success: function(valData, labelData) {
				return valData;
			}
		});

	});
}

//深拷贝
function deepCopy(obj){
	var result = obj instanceof Array? [] : {};
	for (var key in obj) {
		if (obj.hasOwnProperty(key)) {
			if (typeof obj[key] === 'object' && obj[key] !== null) {
				result[key] = deepCopy(obj[key]); //递归复制
			} else {
				result[key] = obj[key];
			}
		}
	}
	return result;
}

if(typeof setChildrenTree != "function") {
    /**
     * 组织多选，选中设置
     * @Allan 2020-07-17
     * @param tree
     * @param selectIds 选中ids 数组
     * @param value
     * @returns {*}
     */
    setChildrenTree  = function(tree,selectIds = [],value = 'id'){
        if(tree != '' && tree != null && tree.length > 0){
            for(var i in tree){
                if($.inArray(tree[i][value],selectIds) != -1 ){
                    tree[i]['selected'] = true;
                }
                if(typeof tree[i]['children'] != "undefined") tree[i]['children'] = setChildrenTree(tree[i]['children'],selectIds,value);
            }
        }
        return tree;
    }
}

if(typeof getXmSelectOrganize != "function") {
    /**
     * 获取组织列表 多选
     * @Allan 2020-07-17
     * @param elem 显示的元素id
     * @param id 选中ids ，逗号隔开
     * @param url
     */
    getXmSelectOrganize = function (elem,id = '') {
        var ids = (id != '') ? id.split(',') : [];
        var url = '/finances/oa/simpleTree/list';
        var index = null;
        layui.use(['layer','form','jquery','xmSelect',], function () {
            layui.form.render();
            var xmSelectObj = xmSelect.render({
                el: '#' + elem,
                toolbar: {show: true},
                filterable: true,
                tree: {
                    //是否显示树状结构
                    show: true,
                    //是否展示三角图标
                    showFolderIcon: true,
                    //是否显示虚线
                    showLine: true,
                    //间距
                    indent: 20,
                    //默认展开节点的数组, 为 true 时, 展开所有节点
                    expandedKeys: [],
                    //是否严格遵守父子模式
                    strict: false,
                    //是否开启极简模式
                    simple: true,
                },
                cascader: {
                    show: false,
                    indent: 100,
                    //是否严格遵守父子模式
                    strict: true,
                },
                name: elem,
                prop:{
                    name : 'name',
                    value : 'id',
                },
                //initValue: ids,
                data: []
            });

            $.ajax({
                url:'/finances/oa/simpleTree/list',
                type:'get',
                async: false,
                beforeSend:function () {
                },
                success:    function (result) {
                    result.data.tree = setChildrenTree(result.data.tree,ids);
                    xmSelectObj.update({
                        data: result.data.tree,
                    })
                },
                complete: function () {
                    layui.form.render();
                }
            });
        });
    }
}

var _hmt = _hmt || [];
(function() {
	var hm = document.createElement("script");
	// hm.src = "https://hm.baidu.com/hm.js?9e39de91fc206c680fb0266ab977e776";
	var s = document.getElementsByTagName("script")[0];
	s.parentNode.insertBefore(hm, s);
})();