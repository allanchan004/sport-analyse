
if(typeof setMouseTips != "function"){
    setMouseTips = function(selector){
        $(document).on('mouseenter', selector, function () {
            var obj = this;
            var msg = $(obj).attr('data-tips');
            if(typeof msg == "undefined" || msg == null || msg == '') return false;
            layer.tips(msg, obj, {
                tipsMore: true,
                time: 0,
                tips: 3
            });

        }).on('mouseleave', selector, function () {
            layer.closeAll();
        });
    }
}

if(typeof getSocialInfo != "function"){
    getSocialInfo = function(staffer_id,callback,isLoad = false){
        fLayer.ajax({
            url: '/finances/basics/functionSelect/socialInfo',
            data: {
                staffer_id:staffer_id,
            },
            isShowLoad: isLoad,
        },function(result){
            if(typeof callback == "function" ) callback(result);
        })
    }
}


/**
 * 是否为数字
 * @param str
 * @Allan 2020-07-06
 * @return {Boolean} true：是，false:否;
 */
if(typeof isNum != "function" ){
    isNum = function(str) {
        var re = /^(-?\d+)(\.\d+)?$/
        return re.test(str);
    }
}
/**
 * 检查数字位数
 * @param value
 * @param len 小数点位数，默认2
 * @Allan 2020-07-06
 * @return {Boolean} true：是，false:否;
 */
if(typeof checkDecimal != "checkDecimal" ){
    checkDecimal = function(value,len = 2){
        var result = true,input = value;
        var ArrMen= input.split(".");    //截取字符串
        if(ArrMen.length == len){
            if(ArrMen[1].length > len){    //判断小数点后面的字符串长度
                result = false;
            }
        }
        return result;
    }
}

/**
 * 检查收起/展开 toggle
 * @Allan 2020-06-16
 */
if(typeof checkSlideToggle != "function" ){
    checkSlideToggle = function(o){
        var obj = o.parents('div.writeBgBlock').find('a.slide-toggle-data');;
        if(obj.attr('default') == 'hide'){
            clickSlideToggle(obj);
            obj.attr('default','hidden');
        }
    }
}
/**
 * 点击收起/展开 toggle
 * @Allan 2020-06-19
 */
if(typeof clickSlideToggle != "function" ){
    clickSlideToggle = function (o) {
        if( typeof o != "object") { console.log(o,'clickSlideToggle 参数错误');return false; }
        var obj = o.parent('span').parent('h5').next('div').find('.layui-table-box');
        o.text(obj.find('.layui-table-body').is(":hidden") ? "收起" : "展开");
        obj.find('.layui-table-body').slideToggle();
        obj.siblings().slideToggle();
    }
}
/**
 * 渲染 审批日志 @Allan 2020-06-15
 * @param logs 日志数据
 * @param objId 表格 渲染ID
 * @param showType 展示内容 1 默认；2 旧字段；
 */
function renderApproveLogs(logs,objId = null,showType = 1){
    if(typeof objId == "undefined" || objId == null || objId == '' ) objId = 'approve_logs_table';
    var cols = [],col = [];

    if(showType == 2) col.push({title: '序号',type: 'numbers',});
    col = col.concat([
        {
            field: 'check_name',minWidth: 80, title: '审批节点',
        }
        , {
            field: 'check_result',minWidth: 80 , title: '审批结果'
        }
        , {
            field: 'user_name',minWidth: 80, title: '审批人',
        }
    ]);

    if(showType == 1) col.push({field: 'jobs_name',minWidth: 80, title: '职位',});
    col = col.concat([
        {
            field: 'remark', title: '审批意见'
        },{
            field: 'create_time',title: '审批时间',minWidth: 180
        }
    ]);
    cols.push(col);

    layui.use(['table','soulTable'], function () {
        var table = layui.table
            ,soulTable = layui.soulTable
            ,$ = layui.$
        ;
        table.render({
            elem: '#' + objId
            , id: objId
            , defaultToolbar: []
            , cellMinWidth: 80
            , totalRow: false
            , fixTotal: true
            , overflow: 'tips'
            , limit: Number.MAX_VALUE
            , cols:cols
            , data: logs
            , done: function (result) {
                soulTable.render(this);
                if(typeof checkSlideToggle == "function") checkSlideToggle($('#'+objId));
            }
        });
    });
}

/**
 * 数组转对象
 * @param arr
 */
function arrayToObject(arr){
    var obj = {};
    if( arr.length > 0 ){
        for(var i in arr){
            obj[arr[i]['name']] = arr[i]['value'];
        }
    }
    return obj;
}

/**
 * 格式化金额.00
 */
function toDecimal2(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return '';
    }
    var f = Math.round(f*100)/100;
    var s = f.toString();
    var rs = s.indexOf('.');
    if (rs < 0) {
        rs = s.length;
        s += '.';
    }
    while (s.length <= rs + 2) {
        s += '0';
    }
    return s;
}

/**
 * 打开窗口
 * @param title 标题
 * @param url 路径
 * @param type 1 打开浮动窗口；2 打开tab窗口
 * @returns {boolean}
 */
function showWindow(title,url,type = 1) {
    type = (typeof type == "undefined" || type == null || type == '') ? 1 : type;
    if( type == 1){
        WeAdminShow(title,url,'','',true)
    }else{
        erp.navTab(url,title,'',true);
    }
    return true;
}
/**
 *  关闭自身 窗口
 * @returns {boolean}
 */
function closeIframe() {
    try{
        //当前是tab 时，关闭tab
        if(typeof parent.layui.admin.events != "undefined") parent.layui.admin.events.closeThisTabs();

        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
        return true;
    }catch (e) {layer.msg('请点击Tab关闭按钮关闭窗口');console.log(e);}
}

/**
 * 打开 窗口
 * @param url
 */
function openWindow(url) {
    window.open(url);
}

/**
 * 打印
 * @param url
 */
function printWindow() {
    window.print();
}