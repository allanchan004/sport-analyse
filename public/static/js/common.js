/**
 * 调用ERP的JS函数
 * @author:Gover_chan
 */
let erp = {
    /**
     * 回调方法收集器
     */
    callback: {},
    /**
     * 触发ERP系统的JS函数
     * @param functionName     调用的方法名
     * @param paramsArr        调用方法的参数，以数组的形式传入
     * @param apply            使用ERP哪个对象调用，默认window
     * @param url              非ERP系统下，不执行ERP的函数，进行跳转的链接
     * @param cbFlag           回调标记,建议存dgdata调用的方法名
     */
    trigger: function (functionName, paramsArr, apply, url, cbFlag) {
        var url = url || '';
        var paramsArr = paramsArr || [];
        var apply = apply || 'window';
        var cbFlag = cbFlag || null;

        if (top.location != self.location) {
            /// @Allan 2020-06-24
            // window.parent.postMessage({
            top.postMessage({
                'function': functionName,
                'params': paramsArr,
                'apply': apply,
                'cbFlag': cbFlag
            }, '*');
        } else {
            if (url !== '') window.open(url, '_blank');
        }
    },

    /**
     * 监听ERp传送过来的数据
     * @param cbFlag            回调标记
     * @param callback          回调函数
     */
    addListen: function (cbFlag, callback) {
        this.callback[cbFlag] = callback;
    },
    /**
     * 启动ERP监听
     */
    listen: function () {
        let _this = this;
        window.addEventListener('message', function (e) {
            let jsonData = e.data;
            let cbFlag = jsonData.cbFlag || null;
            //TODO 判断erp.callback[cbFlag]是否是一个方法
            _this.callback[cbFlag](jsonData.data);
        }, false);
    },
    /**
     * 在ERP的新的选项卡打开
     * @param url
     * @param title
     * @param rel     表单提交成功有需要在ERP关闭的使用form传入
     */
    navTab: function (url, title, rel, erpPage = false) {
        var rel = rel || '_blank';
        this.trigger('erpNavTab', [url, title, rel, erpPage], 'window', url);
    },
    /**
     * 在ERP的弹窗打开
     * @param url
     * @param title
     * @param rel
     */
    open: function (url, title, rel) {
        var rel = rel || 'dgData_flag';

        this.trigger('$.pdialog.open', [url, rel, title, {
            max: false,
            mask: true,
            mixable: true,
            minable: true,
            resizable: true,
            drawable: true,
            fresh: true
        }], 'window', url);
    },
    /**
     * 在ERP中获取地区筛选的数据
     * @param val
     * @param level
     * @param type
     */
    changeArea: function (val, level, type) {
        this.trigger('erpChangeArea', [val, level, type], 'window', '', 'changeArea');
    },
    /**
     * 在ERP中获取地区筛选的数据
     * @param pid
     * @param type
     */
    changeOrganic: function (pid, type) {
        this.trigger('erpChangeOrganic', [pid, type], 'window', '', 'changeOrganic');
    }
}

/**
 * 日期范围控件使用说明：
 * 1 添加class = 'date_input' ，
 * 2 添加data-type为时间选择器的类型，默认值为：date; 参考值：year、month、date、time、datetime
 * 3、name的名字带有begin_前缀和end_前缀作为一对，则为范围选择器
 * @type {{init}}
 * @author:ZZF
 */
let dgdata_input = (function ($) {
    //初始化日期控件
    function date() {
        let date_input_el = $('.date_input');
        let timeTipList = ['start', 'begin'];
        let startTip = '';
        date_input_el.each(function (i, el) {
            let start_el = $(el);
            let startProp = start_el.attr("name");
            let dataType = start_el.data("type") || '';

            for (let i = 0; i < timeTipList.length; i++) {
                if (startProp.indexOf(timeTipList[i]) !== -1) {
                    startTip = timeTipList[i];
                }
            }
            //初始化范围时间控件
            let end_el = ''
            if (startTip != '') {
                endProp = startProp.replace(startTip, 'end');
                if (endProp) {
                    end_el = $("input[name=" + endProp + "]");
                }
                if (end_el.length == 1) {
                    start_el.attr('id', startProp).attr('readonly', 'readonly');
                    end_el.attr('id', endProp).attr('readonly', 'readonly');

                    let start_date = laydate.render({
                        elem: '#' + startProp, //指定元素
                        type: dataType || checkType(start_el.val()),
                        max: new Date().getTime(),
                        done: function (value, date) {
                            end_date.config.min = {
                                year: date.year,
                                month: date.month - 1,
                                date: date.date,
                            }
                        }
                    })
                    let end_date = laydate.render({
                        elem: '#' + endProp, //指定元素
                        type: dataType || checkType(end_el.val()),
                        max: new Date().getTime(),
                        done: function (value, date) {
                            start_date.config.max = {
                                year: date.year,
                                month: date.month - 1,
                                date: date.date,
                            }
                        }
                    })
                }
            }

            //初始化单个时间控件
            if (end_el.length != 1 && startProp != "") {
                start_el.attr('id', startProp).attr('readonly', 'readonly');
                laydate.render({
                    elem: '#' + startProp, //指定元素
                    type: dataType || checkType(start_el.val()),
                    max: new Date().getTime()
                })
            }

        });
    }

    //无data-type属性时检测初始值的类型
    function checkType(val) {
        let typeStr = 'date';  //默认为date;
        let yearType = /^\d{4}$/; //年
        let monthType = /^\d{4}-\d{2}$/;  //月
        let time = /^\d{2}:\d{2}:\d{2}$/;  //时间
        let datetime = /^\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}$/;  //日期加时间
        if (yearType.test(val)) {
            typeStr = 'year';
        } else if (monthType.test(val)) {
            typeStr = 'month';
        } else if (time.test(val)) {
            typeStr = 'time';
        } else if (datetime.test(val)) {
            typeStr = 'datetime';
        }
        return typeStr;
    }

    return {
        init: function (func) {
            $(function () {
                let f = eval(func);
                typeof f === 'function' && f();
            })
        }
    };
})(jQuery);
dgdata_input.init('date');


/**
 * 其它初始化
 * @author:Gover_chan
 */
$(function () {
    let body_el = $('body');

    //便捷跳转:添加data-href属性，便可跳转
    body_el.on('click', '[data-href]', function () {
        let val = $(this).data('href');
        if (!val) return;
        document.location.href = val;
    });

    //便捷打开erp的navTab:添加data-href属性，便可跳转
    var eX = 0;
    var eY = 0;
    var lastX = 0;
    var lastY = 0;
    body_el.on('mousedown', '[data-erp-navtab]', function (e) {
        eX = e.pageX;
        eY = e.pageY;
        let url = $(this).data('erp-navtab');
        let navTitle = $(this).data('erp-title') || $(this).html() + '-页面';
        if (!url) return;

        let flag = escape(url);
        let rel = $(this).data('erp-rel') || flag;
        document.onmouseup = function (ev) {
            lastX = ev.pageX;
            lastY = ev.pageY;

            if (Math.abs(lastX - eX) > 5 || Math.abs(lastY - eY) > 5) {
                // something...
            } else if (ev.button == 0 || ev.button == 1) {
                erp.navTab(url, navTitle, rel);
            }
            document.onmouseup = null;
            eX = 0;
            eY = 0;
            lastX = 0;
            lastY = 0;
        };


    });

    /**
     * 给有title属性的元素，添加data-toggle, 无需要手动添加
     */
    $("[title]").attr("data-toggle", 'tooltip');

    /**
     * 表格排序:添加属性data-order,值为字段名
     * <th data-order="tel">手机号</th>
     */
    let orderName = getQueryString("_order");
    let sortType = getQueryString("_sort");
    $("[data-order]").each(function (i, n) {
        $(this).attr('data-sort', "");
    })
    $("[data-order=" + orderName + "]").attr("data-sort", sortType);
    body_el.on('click', '[data-order]', function () {
        let _this = $(this);
        let order = _this.data('order');
        let sort = _this.data('sort');
        if (sort) {
            sort = sort === 'desc' ? 'asc' : 'desc';
            _this.data('sort', sort);
        } else {
            sort = 'desc';
            _this.data('sort', sort);
        }

        if (!order) return layer.msg('排序字段没有赋值');

        let url = document.location.href;
        url = changeUrlArg(url, '_order', order);
        url = changeUrlArg(url, '_sort', sort);

        document.location.href = url;
    });

    /**
     * zTitle扩展title，鼠标划过显示表格
     * 添加属性:
     *     data-ztitle(数据), 必须设置,值为二维数组
     * 示例: <td data-ztitle='[["姓名","年龄","性别"],["abc","18","男"],["efg","18","女"]]''></td>
     * @author: zefeng.zheng
     */
    body_el.on('mouseover', "[data-ztitle]", function (e) {
        var newTitle = $(this).data("ztitle");
        $('body').append('<div class="ztitleWrap"></div>');
        if (typeof newTitle == 'object') {
            $(".ztitleWrap").css("maxWidth", 'none');
            var iType = 'table';  //目前写死table,之后有需求再写成动态
            switch (iType) {
                case 'table':
                    zTitle.table(newTitle);
                    break;
            }
        } else {
            $('.ztitleWrap').append(newTitle);
        }
        setZtitleXY(e);
        $('.ztitleWrap').css({'visibility': "visible"})
    })
    body_el.on('mousemove', "[data-ztitle]", function (e) {
        setZtitleXY(e);
    })
    body_el.on('mouseout', "[data-ztitle]", function (e) {
        $('.ztitleWrap').remove();
    })
    // zTitle，根据类型生成对应DOM元素
    var zTitle = {
        // 表格类型
        table: function (tList) {
            var tableDom = "<table><thead><tr>";
            tableDom += "</tr></thead><tbody>";
            if (tList) {
                tList.forEach(function (item) {
                    tableDom += "<tr>";
                    item.forEach(function (i) {
                        tableDom += "<td>" + i + "</td>";
                    })
                    tableDom += "<tr>";
                })
                if (tList.length == 1) {
                    var tdLen = tList[0].length;
                    tableDom += "<tr>";
                    tableDom += "<td align='center' colspan='" + tdLen + "'>暂无数据</td>";
                    tableDom += "<tr>";
                }
            }
            tableDom += "</tbody></table>";
            $('.ztitleWrap').append(tableDom);
        }
    }

    // 设置ztitle浮窗的位置
    function setZtitleXY(e) {
        var winW = $(window).width();
        var winH = $(window).height();
        var eW = $(".ztitleWrap")[0].offsetWidth;
        var eH = $(".ztitleWrap")[0].offsetHeight;
        var leftX = eW + e.pageX + 10 > winW ? e.pageX - eW + 10 : e.pageX + 10;
        var topY = eH + e.pageY + 10 > winH ? e.pageY - eH - 10 : e.pageY + 10;
        if (topY < 0) topY = 0;
        $('.ztitleWrap').css({'left': (leftX + 'px'), 'top': (topY + 'px')});
    }

    /**
     * 在当前页面做排列，不做请求
     *
     */
    body_el.on('click', '[data-sort-local]', function () {
        let _this = $(this);
        $("[data-sort-local]").not(_this).attr('data-sort-local', '123456');
        let sort = _this.attr('data-sort-local');
        if (sort) {
            sort = sort === 'desc' ? 'asc' : 'desc';
            _this.attr('data-sort-local', sort);
        } else {
            sort = 'desc';
            _this.attr('data-sort-local', sort);
        }
        sortTable('trueTable', _this.index(), sort)
    });

    function sortTable(classname, rowNum, sortType) {
        var oTable = document.getElementsByClassName(classname)[0];//获取以class为classnametable对象
        var oTbody = oTable.tBodies[0];//获取第一个tBody
        var oRows = oTbody.rows;//获取tBody的所有的行
        var aTRs = new Array;//新建一个数组
        for (var i = 0; i < oRows.length; i++) {
            aTRs[i] = oRows[i];//循环将所有行换到新的数组
        }
        aTRs.sort(function (oTR1, oTR2) {
            var s1 = inAadvance(oTR1.cells[rowNum].firstChild.nodeValue);//比较行的内容的值
            var s2 = inAadvance(oTR2.cells[rowNum].firstChild.nodeValue);//比较行的内容的值
            if (sortType == 'asc') {
                return s1 - s2;//进行比较
            } else {
                return s2 - s1;//进行比较
            }

        });//对数组进行比较,是通过新的数组的行比较
        var otemp = document.createDocumentFragment();//创建文档碎片
        for (var i = 0; i < aTRs.length; i++) {
            if ($("[data-sort-number]").length > 0) {
                console.log($("[data-sort-number]"))
                aTRs[i].cells[0].innerHTML = i + 1;
            }
            otemp.appendChild(aTRs[i]);//分别将每行加入到文档碎片中
        }
        oTbody.appendChild(otemp);//将文档碎片添加到文档中
    }

    // 对数据预先处理
    function inAadvance(value) {
        var isTimeReg = /^\d+[-|/]\d+([-|/]\d+)?$/;  //时间正则
        if (isTimeReg.test(value)) {//字符串为时间格式就转为时间对象
            return new Date(value);
        } else {
            return parseFloat(value);
        }
    }

});

/**
 * 初始操作 @Allan 2020-06-15
 */
$(document).ready(function () {
    //展开，收起表格
    $(document).on("click", ".slide-toggle-data", function () {
        if (typeof clickSlideToggle == "function") clickSlideToggle($(this));
    });
})


/**
 * 替换或者追加URL参数
 * @param url
 * @param arg
 * @param val
 * @returns {string}
 */
function changeUrlArg(url, arg, val) {
    let pattern = arg + '=([^&]*)';
    let replaceText = arg + '=' + val;
    return url.match(pattern) ? url.replace(eval('/(' + arg + '=)([^&]*)/gi'), replaceText) : (url.match('[\?]') ? url + '&' + replaceText : url + '?' + replaceText);
}

/**
 * 根据变量名获取get参数匹配值
 * @param name
 * @returns {*}
 */
function getQueryString(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    let r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return false;
}

//对字符串进行加密
function compileStr(code) {
    var c = String.fromCharCode(code.charCodeAt(0) + code.length);
    for (var i = 1; i < code.length; i++) {
        c += String.fromCharCode(code.charCodeAt(i) + code.charCodeAt(i - 1));
    }
    return escape(c);
}

//字符串进行解密
function uncompileStr(code) {
    code = unescape(code);
    var c = String.fromCharCode(code.charCodeAt(0) - code.length);
    for (var i = 1; i < code.length; i++) {
        c += String.fromCharCode(code.charCodeAt(i) - c.charCodeAt(i - 1));
    }
    return c;
}


/**
 * 设置select默认值
 * @param name
 * @param val
 */
function setDefaultSelect(name, val) {
    $('select[name=' + name + ']').find('option[value=' + val + ']').attr('selected', true)
}

/**
 * 获取表单JSON格式数据
 * @param id
 */
function getFormJson(id) {
    var d = {};
    var t = $('#' + id).serializeArray();
    $.each(t, function () {
        d[this.name] = this.value;
    });
    return d;
}

// 是否显示更多搜索选项,更多里面的元素都需要加类“.search-msg”
function isShowMore() {
    var searchMsgNum = $('.layui-card-more .search-msg').size();
    if (searchMsgNum > 8) {
        $('.more-btn').show();
        $('.search-msg').eq(7).nextAll().addClass('search-msg-hidden');
        $('.layui-card-more').css({'height': 'auto', 'padding-bottom': '23px'});
    } else {
        $('.more-btn').hide();
        $('.layui-card-more').css('height', 'auto');
    }
}

function layerSuccMsg(msg, callback = null) {
    layer.msg(msg, {
        icon: 1,
        time: 2000,
        shade: 0.01,
    }, callback);
}

function layerErrMsg(msg, callback = null) {
    layer.msg(msg, {
        icon: 2,
        time: 2000
    }, callback);
}

function layerAlertMsg(msg, callback = null) {
    layer.alert(msg, {
        icon: 2
    }, callback);
}

function layerLoad() {
    return layer.load(0, {shade: [0.01, '#fff']});
}

//重置table表单数组name
function resetTableFormName() {
    $(".table_tr").each(function () {
        $(this).find("[name]").each(function () {
            var name = $(this).attr('name');
            if (name.indexOf("[") === -1) {
                return true;
            }

            var _arr = name.split("[");
            name = _arr[0] + '[]';

            $(this).attr('name', name);
        });
    });
    resetApprovalflowForm();
};

function resetApprovalflowForm(){
    var i = 0;
    $("#approvalflow").find('.mul_approval_user').each(function () {
        $(this).find("[name]").each(function () {
            var name = $(this).attr('name');
            if (name.indexOf("[") === -1) {
                return true;
            }

            var _arr = name.split("[");
            name =  _arr[0] +( _arr[0] == 'approvalflow' ? ('['+ i +'][]') : '[]' );

            $(this).attr('name', name);
        });
        i++;
    });
}

//more跳转
function moreJump($selected, $url, $title = '更多详情') {
    $($selected).on('click', function () {
        // parent.parent.erpNavTab($url+
        //     , $title
        // );
        // closeIframe();
        WeAdminShow($title, $url);
    })
}

//千分位
function thousands(num) {
    var number = parseFloat(num);
    if (isNaN(number)) return num;
    var nn = number.toLocaleString('en-US');
    if (nn.indexOf(".") == -1) {
        nn = nn + ".00";
    } else {
        nn = nn.split(".")[1].length < 2 ? nn + "0" : nn;
    }
    return nn;
}

//侧边导航
function sidenav() {
    $('.weadmin-body').after('<div class="oaBookmarkBox"><ul></ul></div>');
    $('.contentAuto .writeBgBlock').each(function (index) {
        var id = $(this).attr('id');
        var text = $(this).children('.titleHeader').clone().children().remove().end().text();
        var li = '';
        if (index == 0)
            li = '<li data-url="' + id + '" class="active">' + text + '</li>';
        else
            li = '<li data-url="' + id + '">' + text + '</li>';
        $('.oaBookmarkBox>ul').append(li);
    })

    $(".oaBookmarkBox>ul>li").on('click', function () {
        $(this).addClass('active')
            .siblings('li').removeClass('active');
        var thisUrl = $(this).attr("data-url");
        document.getElementById(thisUrl).scrollIntoView();
    });
}

//修复数据表格fixed高度
function fixedHeight(layId) {
    var obj = $("div[lay-id=" + layId + "]");
    var hh = obj.find(".layui-table-header:first tr").height();
    var hb = [];
    obj.find(".layui-table-body:first tr").each(function (index, item) {
        hb[index] = $(item).height();
    });
    obj.find(".layui-table-fixed").each(function (index, item) {
        $(item).find(".layui-table-header:first tr").height(hh);
        $(item).find(".layui-table-body:first tr").each(function (index, item) {
            $(item).height(hb[index]);
        });
    });
}

// 多重选择挂载*
(function (window, factory) {
    if (typeof exports === 'object') { // 支持 CommonJS
        module.exports = factory();
    } else if (typeof define === 'function' && define.amd) { // 支持 AMD
        define(factory);
    } else {
        window.manySelects = factory();
    }
})(window, function () {


// 多重选择构造函数*
    var ManySelectFrom = function (data) {
        this.data = data;//数据源
        this.manySelect;//many-select的值
        this.selectBox;//select的class
        this.arrName = [];//被选择的name数组
    }


    /**
     * 多重选择外面调用的传值
     * @param  {[type]} manySelect [many-select的值，用于判断渲染该地方]
     * @param  {[type]} selectBox [selectBox为select的class]
     * @param  {[type]} data       [数据源]
     * @return {[type]}            [description]
     */
    ManySelectFrom.prototype.initManySelect = function (manySelect, selectBox, data) {//还没完成
        var _this = this;
        _this.data = data;
        _this.manySelect = manySelect;
        _this.selectBox = selectBox;
        if (_this.manySelect != 'manySelect16') {
            return;
        }
        this.renderSelect();  //渲染选择框项
        this.liClick();//渲染事件
    }

// 多重选择渲染select大盒子
    ManySelectFrom.prototype.renderSelect = function () {
        var _this = this;
        $(`.${_this.selectBox}`).hide();//隐藏select option
        var selectedForm = `<div class='selected_form'></div>`;
        $(`.${_this.selectBox}`).after(selectedForm);
        var selectedFormInput = `<div class="selected_form_input">
                                <div class="selected_form_input_div">
                                  <div class="selected_form_input_div_label">
                                    <span>
                                      <font></font>
                                      <i href="#" class="many_close"></i>
                                    </span>
                                  </div>
                                </div>
                              </div>`;
        $('.selected_form').append(selectedFormInput);

        var select16 = `<dl class="select16 many_select_dl ">
                          <div class="one_grade"></div>
                      </div>`;
        $('.selected_form').append(select16);

        _this.data.forEach(function (element, index) {
            // 一级
            var selectHtmlOne = `<li xm-value=${element.value} name=${element.name}>${element.name}</li>`;
            $('.one_grade').append(selectHtmlOne);

            if (element.children.length > 0) {
                _this.renderChild(element.children, element)
            }
        });
    }

    /**
     * 多重选择渲染子选项
     * @param  {[type]} data [子数据]
     * @param  {[type]} pEle [父数据]
     * @return {[type]}      [no]
     */
    ManySelectFrom.prototype.renderChild = function (data, pEle) {
        var _this = this;
        var childClass = $(".select16").children().hasClass(`grade${data[0].level}`);
        if (!childClass) {
            var gradeLevel = `<div class="grade${data[0].level} xm-select-linkage-hide"></div>`;
            $('.select16').append(gradeLevel);
        }

        data.forEach(function (ele, indexTwo) {
            // 二级
            var selectHtmlTwo = `<li pid=${pEle.value} name=${ele.name} class="xm-select-linkage-hide" xm-value=${ele.value}>${ele.name}</li>`;
            $(`.grade${data[0].level}`).append(selectHtmlTwo);

            if (ele.children.length > 0) {
                _this.renderChild(ele.children, ele)
            }
        });
    }

// 多重选择渲染事件
    ManySelectFrom.prototype.liClick = function () {
        var _this = this;

        // 所有li
        $('.select16').on('click', 'li', function () {
            // 当前id
            var nowId = $(this).attr('xm-value');
            $(this).parent().nextAll().addClass('xm-select-linkage-hide');
            if ($(`[pid|=${nowId}]`).length > 0) {
                $(this).parent().next().removeClass('xm-select-linkage-hide');
            }
            $(this).parent().next().find('li').addClass('xm-select-linkage-hide');
            $(`[pid|=${nowId}]`).removeClass('xm-select-linkage-hide');

            $('.selected_form_input_div_label').find('span').css('display', 'block');

            _this.arrName = [];
            var nowName = $(this).attr('name');// 获取当前name
            _this.arrName.push(nowName)

            // 父级id
            var pid = $(this).attr('pid');
            // 无限获取父
            if (pid != undefined) {
                var parentName = _this.findParent(pid);
            }
            _this.arrName = _this.arrName.reverse();
            select16Give(_this.arrName)
        })

        /**
         * 赋值选项框
         */
        function select16Give(selectArr) {
            var valueString = selectArr.join('/');
            $('.selected_form_input .selected_form_input_div_label').find('font').html(valueString);
        }

        // 值事件
        $('.selected_form_input').on('click', function (e) {
            e.stopPropagation();
            var display = $('.many_select_dl').css('display');
            if (display === 'none') {
                $('.many_select_dl').css('display', 'inline-flex');
            } else {
                $('.many_select_dl').css('display', 'none');
            }
        })

        // 隐藏阻止事件冒泡
        $('.select16').on('click', function (e) {
            e.stopPropagation();
        })

        // 点击隐藏选项
        $(document).click(function () {
            $('.many_select_dl').css('display', 'none');
        });

        /**
         * 关闭选项
         */
        $('.many_close').on('click', function (e) {
            e.stopPropagation();
            $('.selected_form_input_div_label').find('span').css('display', 'none');
            _this.arrName = [];
            select16Give(_this.arrName);
        })

    }

// 多重选择获取父方法
    ManySelectFrom.prototype.findParent = function (pid) {
        var _this = this;
        var pName = $(`[xm-value|=${pid}]`).attr('name');//获取父级name
        _this.arrName.push(pName);

        var ppid = $(`[xm-value|=${pid}]`).attr('pid');
        if (ppid != undefined) {
            _this.findParent(ppid)
        }
    }

    var ManySelect = new ManySelectFrom();
    return ManySelect;
});

/**
 * 多重选择调用方法例子
 * 外面操作的manySelects.initManySelect方法
 * @param  {[string]} 参数1 [many-select16 ，必须的]
 * @param  {[string]} 参数2 [select的类，会挂载在这下面]
 * @param  {[json]} 参数3 [参数数据，格式如下]
 * {
      "name": "天津",
      "value": 2,
      "level":1,
      "children": [
          {"name": "天津市1", "level":2,"value": 51, "children": []},
      ]
   }
 */
// manySelects.initManySelect('manySelect16','select_box',arr)

var showClick = 0;
function tableSlideToggle(clickClassName,HideClassName)
{
    var clickEle = $('.' + clickClassName)
    var todoEle = $('.' + HideClassName)

    if (0 === showClick) {
        showClick = 1
        clickEle.text(todoEle.is(':hidden') ? '收起' : '展开')
        todoEle.slideToggle()
        setTimeout(function () {
            showClick = 0
        }, 800);
    }
}