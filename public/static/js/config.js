/**
 * 参数配置
 * @author Allan
 */
//config的设置是全局的
//layui 模块扩展
let host = window.location.protocol + "//" + window.location.host;
let baseUrl = host + '/static/js/layui_extends/';
layui.config({//配置 layui 第三方扩展组件存放的基础目录
    base: '/static/js/layui_extends/',
}).extend({ //设定模块别名
    // 无限级联选择器
    cascader: '{/}' + baseUrl + 'cascader/cascader',

    // 多级列表
    dtree: '{/}' + baseUrl + 'dtree/dtree',

    // 多级表格
    treeTable: '{/}' + baseUrl + 'treeTable/treeTable.min',

    //标签
    tag: '{/}' + baseUrl + 'lih_tag/tag',

    //多选
    selectN: '{/}' + baseUrl + 'selectM/selectN',
    selectM: '{/}' + baseUrl + 'selectM/selectM',
    xmSelect: '{/}' + baseUrl + 'selectM/xm-select',

    // soulTable.js 总入口
    // tableFilter.js 表头筛选
    // excel.js excel导出
    // tableChild.js 子表
    // tableMerge.js 单元格合并
    soulTable: '{/}' + baseUrl + 'soulTable/soulTable.min',
    tableChild: '{/}' + baseUrl + 'soulTable/tableChild.min',
    tableMerge: '{/}' + baseUrl + 'soulTable/tableMerge.min',
    tableFilter: '{/}' + baseUrl + 'soulTable/tableFilter.min',
    excel: '{/}' + baseUrl + 'soulTable/excel.min',


});

// console.log(layui.cache.base,'layui.cache.base')
// console.log(host,'host')
// console.log(baseUrl,'baseUrl')