/**
 * layer 封装 @copy right by OA组
 * @Allan 2020-06-19
 * @type {{uiIndex: string, errorTips: fLayer.errorTips, closeIframe: fLayer.closeIframe, loading: (function(*=): string), error: (function(*): *), ajax: fLayer.ajax, tips: fLayer.tips, successAlert: (function(*=, *=, *=): *), errorAlert: (function(*=, *=, *=): *), reload: fLayer.reload, alert: (function(*=, *=, *=): *), successTips: fLayer.successTips, close: fLayer.close}}
 */
let fLayer = {
    //存储遮罩层Index
    uiIndex: '',
    //打开加载遮罩层
    loading: function(msg = '数据处理中...'){
        this.uiIndex = layui.layer.msg(msg, {icon: 16,shade: [0.5],scrollbar: false,offset: 'auto',time: 0,zIndex:19891019});
        return this.uiIndex;
    },

    //关闭遮罩层
    close: function(index = ''){
        layui.layer.close( index != '' ? index : this.uiIndex);
    },
    //重载
    reload: function( filter = '', where = {} ,page = null){
        layui.use(['table'], function(){
            var table = this.table;
            var param = {
                where: where,
            };
            if(page != null ) param.page.curr = page;
            table.reload(filter,param);
        })
    },

    /**
     *异步请求公用方法
     * @param request 对象
     * @param callback 回调函数
     * @param comCallback 完成回调函数
     */
    ajax: function (request = {},callback,completeCallback) {
        var that = this;
        $.ajax({
            type: typeof request.type != "undefined" ? request.type :'post',
            url: request.url,
            data: request.data,
            dataType: typeof request.dataType != "undefined" ? request.dataType : 'json',
            beforeSend: function () {
                //是否显示 加载
               if(typeof request.isShowLoad == "undefined" || request.isShowLoad == true ) that.loading();
            },
            success:function(result){
                typeof callback === 'function' && callback(result);
            },
            error: function (error) {
                that.error(error);
            },
            complete: function (XMLHttpRequest) {
                that.close();
                typeof completeCallback === 'function' && completeCallback(XMLHttpRequest);
            }
        });
    },

    /**
     *异常消息处理
     */
    error: function (error) {
       return layui.layer.open({
            type: 1,
            title:[ error.status+' ' + error.statusText,'font-size:20px;color:red'],
            area: ['500px', '360px'],
            shadeClose: false,
            content: '\<\div style="padding:20px;">'+error.responseText+'\<\/div>'
        });
    },

    tips:function(message,icon = 0){
        layui.layer.msg(message, {icon: icon,scrollbar: false});
    },

    /***
     * 错误消息提示
     * @param message
     */
    errorTips: function(message){
        this.tips(message,2);
    },

    /**
     * 成功消息提示
     * @param message
     */
    successTips: function(message){
        this.tips(message,1);
    },

    /**
     * 消息弹窗
     * @param message 提示内容
     */
    alert: function(message,callback, title = '提示',icon = 3){
        return layui.layer.alert(message, {
            title: title,
            icon: icon,
            area: ['350px', 'auto'],
            btn: ['知道了'],
            btnAlign: 'c',
            end: callback
        }, function(index){
            layui.layer.close(index);
        });
    },

    /**
     * 成功消息弹窗
     * @param message
     * @param callback
     * @param title
     */
    successAlert: function(message, callback, title = '成功'){
        return this.alert(message,callback,title,1)
    },

    /**
     * 错误消息弹窗
     * @param message
     * @param callback
     * @param title
     */
    errorAlert:function(message, callback = null, title = '警告'){
        return this.alert(message,callback,title,2)
    },

    /**
     * 关闭iframe窗口
     */
    closeIframe: function(){
        var index = parent.layui.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    },

    go: function(url){
        window.location.href = url;
    },
};