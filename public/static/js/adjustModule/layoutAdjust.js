$(document).ready(function() {
    $('.all-search').each(function(){
    	var show_type = $(this).attr('show_type');
		show_type = (typeof show_type == "undefined" || show_type == null ) ? 1 : show_type;
		moreFilter($(this),show_type);
    })
});

function getAllSearch(a,n,o){
	var moreBtnVal = false;
	if(typeof o != "object") o = $(".all-search");

	var moreBtnO = o.parents(".layui-card-more").find(".more-btn");
	moreBtnO.show();
	o.find(".layui-inline").eq(a-n).nextAll().addClass('hideClass');
	moreBtnO.on('click', function() {
		if (!moreBtnVal) {
			moreBtnO.text('收起筛选条件');
			o.find(".layui-inline").eq(a-n).nextAll().toggleClass('hideClass');
		} else {
			moreBtnO.text('更多筛选条件');
			o.find(".layui-inline").eq(a-n).nextAll().addClass('hideClass');
		}
		moreBtnVal = !moreBtnVal;
	})
};

/**
 * 更多筛选条件
 * @param o
 * @param show_type 显示方式 1 默认显示两行；2 显示一行；
 */
function moreFilter(o,show_type = 1){
	if(typeof o != "object") o = $(".all-search");
	var clientWidth = document.body.clientWidth,
			len = o.children('.layui-inline').length,
			leng = o.find(".organizeModule").length;
	var showMax = 8;// 第一版最多显示数量

	if(clientWidth < 1160 ){
		// $('body').css('min-width','845px');//限制最小宽度
		showMax = 4;
	}else if( clientWidth < 1480 ){
		showMax = 6;
	}else{
		showMax = 8;
	}

	//@Allan 2020-06-18
	var max_width = (showMax/2) * 313 + 100;
	o.css('max-width', max_width + 'px');//限制最大宽度

	// @Allan 2020-08-31 只显示一行搜索框
	if(show_type == 2) showMax /= 2;

	var end = len >= showMax ? showMax : len;
	var hS = 0,index = end;// 开始隐藏的下标，默认 第8个以后
	for(var i = 0;i < end;i++ ){//计算 前8个 div有几个 organizeModule 样式
		var obj = o.find(".layui-inline").eq(i);
		if( obj.find('.organizeModule').html() != undefined ) {//存在 organizeModule div
			hS +=2;
		}else{
			hS++;
		}

		index = i;
		if( hS >=  showMax ) break;
	}
	// console.log(max_width,'max_width')
	// console.log(clientWidth,'clientWidth')
	// console.log(showMax,'showMax')
	// console.log(hS,'hS')
	// console.log(index,'index')

	if( hS > showMax ) index--;// div 有溢出，减少显示一个div
	if( index < (len -1) ) getAllSearch(index,0,o);//需要隐藏的div
	if( index == (len -1) ) o.parents(".layui-card-more").find(".more-btn").hide();//div 全部可在第一版展示，隐藏 more-btn

}

/**
 * 清空按钮 @lidao
 */
emptyForm = function () {
	$("#select-form").find('input[type=text],input[type=number],select,.lastLeveVal,.selectedLeveVal,textarea').each(function() {
		$(this).val('');
	});
	$('.xm-icon-qingkong').click();
	layui.form.render();
}

//清空条件功能
$(".clearBtn").on('click',function(){
	emptyForm();
});

