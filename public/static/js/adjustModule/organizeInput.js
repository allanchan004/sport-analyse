layui.use(['laytpl','layer','form'], function () {
	    var laytpl = layui.laytpl,
					layer = layui.layer,
	        form = layui.form;
			//点击组织架构文本框，弹出弹窗		
			$("#organizeInput").on('click',function(e){
					e.stopPropagation();
				  var index = layer.open({
						type: 1,
						title: '组织架构',
						area: [ '600px', '420px' ],
						id: 'organize',
						fix: false,
						btn: ['确定', '取消'],
						btnAlign: 'c',
						offset: '150px',
						content: $("#organizeBox"),
						yes: function(layero, index){
								// form.render();
								var departmentSelectVal = $("#department_select>.layui-input-inline:last").find('.layui-select-title .layui-unselect').val();
								$("#organizeInput").val(departmentSelectVal);
								layer.closeAll();
						}
				})
			});		
			//点击文档其他地方关闭弹窗
			$(document).on('click',function(e){
					var e = e || window.event; //浏览器兼容性
					var elem = e.target || e.srcElement;
					while (elem) {
							if (elem.id && elem.id=='organize') {
									return;
							}
							elem = elem.parentNode;
					}
					layer.close(layer.index);
			});
			
	});