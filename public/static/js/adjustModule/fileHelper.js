$(function(){
	dragPanelMove("#recordImg","#oaFileHelperBox");
	$("#recordImg").on('click',function(){
		$("#upDownIcon").toggleClass('toggleIcon',300);
		$(".oaFixPopBox").slideToggle(300);
	})
	
	$("#recordImg").hover(function(){
			$(".hoverMarkBox").show();
	},function(){
			$(".hoverMarkBox").hide();
	});
	
	$("#upDownIcon").hover(function(){
		var text = $(this).text();
			if(gblen(text)>19){
				$("#nameSpill").show();
			}else{
				$("#nameSpill").hide();
			}
	},function(){
			$("#nameSpill").hide();
	});
	
	$(".oaFixTopBox span").hover(function(){
		var text = $(this).text();
			if(gblen(text)>50){
				$("#organizationSpill").show();
			}else{
				$("#organizationSpill").hide();
			}
	},function(){
			$("#organizationSpill").hide();
	});
	
});

function gblen(str){ 
    let len = 0;
    for (let i = 0; i < str.length; i++) {
      if (str.charCodeAt(i) > 127 || str.charCodeAt(i) === 94) {
        len += 2;
      } else {
        len++;
      }
    }
    return len;
}
	
function dragPanelMove(downDiv,moveDiv){
	$(downDiv).mousedown(function (e) {
		var isMove = true;
		var div_x = e.pageX - $(moveDiv).offset().left;
		var div_y = e.pageY - $(moveDiv).offset().top;
		
		$(document).mousemove(function (e) {
				// if (isMove) {
				// 		var obj = $(moveDiv);
				// 		var clientHeight = document.documentElement.clientHeight - 100;
						
				// 		if(e.pageY - div_y > clientHeight){
				// 			obj.css({"left":e.pageX - div_x, "top":clientHeight});
				// 		}else{
				// 			obj.css({"left":e.pageX - div_x, "top":e.pageY - div_y});
				// 		}
				// }
				if (isMove) {
				    var obj = $(moveDiv);
				    var clientHeight = document.documentElement.clientHeight - 100;
				    var css_top;
				    var css_left = e.pageX - div_x;
				    if (e.pageY - div_y > clientHeight) {
				        css_top = clientHeight;
				    } else {
				        css_top = e.pageY - div_y;
				    }
				    css_top <= 0 ? css_top = 0 : '';
				    css_left <= 0 ? css_left = 0 : '';
				    obj.css({"left": css_left, "top": css_top});
				}
		}).mouseup(
				function () {
				isMove = false;
		});
	});
}

						