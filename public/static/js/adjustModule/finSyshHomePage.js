$(function(){
		finSysMainBoxDate();
		
		layui.config({
				base:'/admin/js/'
		}).use(['element', 'echarts','table'], function() {
			var element = layui.element,
			$ = layui.jquery,
			echarts = layui.echarts,
			table = layui.table;
			
			// 账户资金图表
			var capitalAccount = echarts.init(document.getElementById('capitalAccount'));
			capitalAccountOption = {
					    tooltip: {
					        trigger: 'axis',
					        axisPointer: {
					            type: 'shadow'
					        }
					    },
					    legend: {
								right:'left',
								top:'top',
					      data: ['收入', '支出','余额'],
								itemWidth:10,//图例的宽度
								itemHeight:10,//图例的高度
								textStyle:{
										color:'#fff',
										fontSize:10
								}
					    },
					    grid: {
					        left: '0',
					        right: '18%',
					        bottom: '2%',
					        containLabel: true
					    },
					    xAxis: {
									name:'金额: 元',
									nameTextStyle:{fontSize: 10},
					        type: 'value',
					        boundaryGap: [0, 0.01],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisLabel: {
										  show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    yAxis: {
					        type: 'category',
									nameTextStyle:{fontSize: 10},
					        data: ['通道', '银行', '全部'],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    series: [
					        {
					            name: '收入',
					            type: 'bar',
					            data: [18203, 23489, 29034],
											itemStyle: {
													normal: {
														color: '#36e771',
														label:{
															show:true,
															position:'right'
														}
													}
											}
					        },
					        {
					            name: '支出',
					            type: 'bar',
					            data: [19325, 23438, 31000],
											itemStyle: {
													normal: {
														  color: '#fd5a28',
															label:{
																show:true,
																position:'right'
															}
													}
											}
					        },
									{
									    name: '余额',
									    type: 'bar',
											barGap: 0,
											barWidth: 18,
									    data: [25325, 15768, 16700],
											itemStyle: {
													normal: {
														  color:'#249cf9',
															label:{
																show:true,
																position:'right'
															}
													}
											}
									}
					    ]
			};
			capitalAccount.setOption(capitalAccountOption, true);
			
			// 账户资金图表
			var capitalAccount1 = echarts.init(document.getElementById('capitalAccount1'));
			capitalAccountOption1 = {
					    tooltip: {
					        trigger: 'axis',
					        axisPointer: {
					            type: 'shadow'
					        }
					    },
					    legend: {
								right:'left',
								top:'top',
					      data: ['收入', '支出','余额'],
								itemWidth:10,//图例的宽度
								itemHeight:10,//图例的高度
								textStyle:{
										color:'#fff',
										fontSize:10
								}
					    },
					    grid: {
					        left: '0',
					        right: '18%',
					        bottom: '2%',
					        containLabel: true
					    },
					    xAxis: {
									name:'金额: 元',
									nameTextStyle:{fontSize: 10},
					        type: 'value',
					        boundaryGap: [0, 0.01],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisLabel: {
										  show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    yAxis: {
					        type: 'category',
									nameTextStyle:{fontSize: 10},
					        data: ['通道', '银行', '全部'],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    series: [
					        {
					            name: '收入',
					            type: 'bar',
					            data: [18203, 23489, 29034],
											itemStyle: {
													normal: {
														color: '#36e771',
														label:{
															show:true,
															position:'right'
														}
													}
											}
					        },
					        {
					            name: '支出',
					            type: 'bar',
					            data: [19325, 23438, 31000],
											itemStyle: {
													normal: {
														  color: '#fd5a28',
															label:{
																show:true,
																position:'right'
															}
													}
											}
					        },
									{
									    name: '余额',
									    type: 'bar',
											barGap: 0,
											barWidth: 18,
									    data: [25325, 15768, 16700],
											itemStyle: {
													normal: {
														  color:'#249cf9',
															label:{
																show:true,
																position:'right'
															}
													}
											}
									}
					    ]
			};
			capitalAccount1.setOption(capitalAccountOption1, true);
			
			// 账户资金图表
			var capitalAccount2 = echarts.init(document.getElementById('capitalAccount2'));
			capitalAccountOption2 = {
					    tooltip: {
					        trigger: 'axis',
					        axisPointer: {
					            type: 'shadow'
					        }
					    },
					    legend: {
								right:'left',
								top:'top',
					      data: ['收入', '支出','余额'],
								itemWidth:10,//图例的宽度
								itemHeight:10,//图例的高度
								textStyle:{
										color:'#fff',
										fontSize:10
								}
					    },
					    grid: {
					        left: '0',
					        right: '18%',
					        bottom: '2%',
					        containLabel: true
					    },
					    xAxis: {
									name:'金额: 元',
									nameTextStyle:{fontSize: 10},
					        type: 'value',
					        boundaryGap: [0, 0.01],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisLabel: {
										  show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    yAxis: {
					        type: 'category',
									nameTextStyle:{fontSize: 10},
					        data: ['通道', '银行', '全部'],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    series: [
					        {
					            name: '收入',
					            type: 'bar',
					            data: [18203, 23489, 29034],
											itemStyle: {
													normal: {
														color: '#36e771',
														label:{
															show:true,
															position:'right'
														}
													}
											}
					        },
					        {
					            name: '支出',
					            type: 'bar',
					            data: [19325, 23438, 31000],
											itemStyle: {
													normal: {
														  color: '#fd5a28',
															label:{
																show:true,
																position:'right'
															}
													}
											}
					        },
									{
									    name: '余额',
									    type: 'bar',
											barGap: 0,
											barWidth: 18,
									    data: [25325, 15768, 16700],
											itemStyle: {
													normal: {
														  color:'#249cf9',
															label:{
																show:true,
																position:'right'
															}
													}
											}
									}
					    ]
			};
			capitalAccount2.setOption(capitalAccountOption2, true);
			
			var capitalAccount4 = echarts.init(document.getElementById('capitalAccount4'));
			capitalAccountOption4 = {
					    tooltip: {
					        trigger: 'axis',
					        axisPointer: {
					            type: 'shadow'
					        }
					    },
					    legend: {
								right:'left',
								top:'top',
					      data: ['收入', '支出','余额'],
								itemWidth:10,//图例的宽度
								itemHeight:10,//图例的高度
								textStyle:{
										color:'#fff',
										fontSize:10
								}
					    },
					    grid: {
					        left: '0',
					        right: '18%',
					        bottom: '2%',
					        containLabel: true
					    },
					    xAxis: {
									name:'金额: 元',
									nameTextStyle:{fontSize: 10},
					        type: 'value',
					        boundaryGap: [0, 0.01],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisLabel: {
										  show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    yAxis: {
					        type: 'category',
									nameTextStyle:{fontSize: 10},
					        data: ['通道', '银行', '全部'],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    series: [
					        {
					            name: '收入',
					            type: 'bar',
					            data: [18203, 23489, 29034],
											itemStyle: {
													normal: {
														color: '#36e771',
														label:{
															show:true,
															position:'right'
														}
													}
											}
					        },
					        {
					            name: '支出',
					            type: 'bar',
					            data: [19325, 23438, 31000],
											itemStyle: {
													normal: {
														  color: '#fd5a28',
															label:{
																show:true,
																position:'right'
															}
													}
											}
					        },
									{
									    name: '余额',
									    type: 'bar',
											barGap: 0,
											barWidth: 18,
									    data: [25325, 15768, 16700],
											itemStyle: {
													normal: {
														  color:'#249cf9',
															label:{
																show:true,
																position:'right'
															}
													}
											}
									}
					    ]
			};
			capitalAccount4.setOption(capitalAccountOption4, true);
			
			var capitalAccount5 = echarts.init(document.getElementById('capitalAccount5'));
			capitalAccountOption5 = {
					    tooltip: {
					        trigger: 'axis',
					        axisPointer: {
					            type: 'shadow'
					        }
					    },
					    legend: {
								right:'left',
								top:'top',
					      data: ['收入', '支出','余额'],
								itemWidth:10,//图例的宽度
								itemHeight:10,//图例的高度
								textStyle:{
										color:'#fff',
										fontSize:10
								}
					    },
					    grid: {
					        left: '0',
					        right: '18%',
					        bottom: '2%',
					        containLabel: true
					    },
					    xAxis: {
									name:'金额: 元',
									nameTextStyle:{fontSize: 10},
					        type: 'value',
					        boundaryGap: [0, 0.01],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisLabel: {
										  show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    yAxis: {
					        type: 'category',
									nameTextStyle:{fontSize: 10},
					        data: ['通道', '银行', '全部'],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    series: [
					        {
					            name: '收入',
					            type: 'bar',
					            data: [18203, 23489, 29034],
											itemStyle: {
													normal: {
														color: '#36e771',
														label:{
															show:true,
															position:'right'
														}
													}
											}
					        },
					        {
					            name: '支出',
					            type: 'bar',
					            data: [19325, 23438, 31000],
											itemStyle: {
													normal: {
														  color: '#fd5a28',
															label:{
																show:true,
																position:'right'
															}
													}
											}
					        },
									{
									    name: '余额',
									    type: 'bar',
											barGap: 0,
											barWidth: 18,
									    data: [25325, 15768, 16700],
											itemStyle: {
													normal: {
														  color:'#249cf9',
															label:{
																show:true,
																position:'right'
															}
													}
											}
									}
					    ]
			};
			capitalAccount5.setOption(capitalAccountOption5, true);
			
			var capitalAccount6 = echarts.init(document.getElementById('capitalAccount6'));
			capitalAccountOption6 = {
					    tooltip: {
					        trigger: 'axis',
					        axisPointer: {
					            type: 'shadow'
					        }
					    },
					    legend: {
								right:'left',
								top:'top',
					      data: ['收入', '支出','余额'],
								itemWidth:10,//图例的宽度
								itemHeight:10,//图例的高度
								textStyle:{
										color:'#fff',
										fontSize:10
								}
					    },
					    grid: {
					        left: '0',
					        right: '18%',
					        bottom: '2%',
					        containLabel: true
					    },
					    xAxis: {
									name:'金额: 元',
									nameTextStyle:{fontSize: 10},
					        type: 'value',
					        boundaryGap: [0, 0.01],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisLabel: {
										  show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    yAxis: {
					        type: 'category',
									nameTextStyle:{fontSize: 10},
					        data: ['通道', '银行', '全部'],
									axisLine: {
												lineStyle: {
													color: '#fff'
											},
									},
									splitLine: {
											show: false,
									},
									axisTick: {
										  show: false,
									}
					    },
					    series: [
					        {
					            name: '收入',
					            type: 'bar',
					            data: [18203, 23489, 29034],
											itemStyle: {
													normal: {
														color: '#36e771',
														label:{
															show:true,
															position:'right'
														}
													}
											}
					        },
					        {
					            name: '支出',
					            type: 'bar',
					            data: [19325, 23438, 31000],
											itemStyle: {
													normal: {
														  color: '#fd5a28',
															label:{
																show:true,
																position:'right'
															}
													}
											}
					        },
									{
									    name: '余额',
									    type: 'bar',
											barGap: 0,
											barWidth: 15,
									    data: [25325, 15768, 16700],
											itemStyle: {
													normal: {
														  color:'#249cf9',
															label:{
																show:true,
																position:'right'
															}
													}
											}
									}
					    ]
			};
			capitalAccount6.setOption(capitalAccountOption6, true);
			
			table.render({
			    elem: '#banklist'
			    ,id:'banklist'
			    ,url: "/finances/basics/bank/index" //数据接口
			    ,cellMinWidth: 80
			    ,page: false //开启分页
			    ,method:'post'
					,totalRow: true
			    ,limit:50
					,height: '470'//不含标签页,不含日报，月报，年报的高度
					// ,height: 'full-290' //含有tab标签页的高度
					// ,height: 'full-280' //含有日报，月报，年报的高度
			    ,initSort: {
			        field: 'fd_account_id' //排序字段，对应 cols 设定的各字段名
			        ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
			    }
			    ,sort:true
			    ,cols: [[ //表头
			        {
								fixed: 'left',
			          type: 'numbers',
								title:'序号',
								width:80
			        }
			        ,{field: 'fd_account_isstop', title: '区域', align:'center', unresize: true, totalRowText: '合计'}
			        ,{field: 'fd_account_username', title: '收入(一级代理)', align:'center', totalRow: true}
			        ,{field: 'fd_account_money', title: '成本(费用+工资)', align:'center', totalRow: true}
			    ]]
			    ,done:function () {
			        $("[data-field='fd_account_id']").css('display','none');
			    }
			    ,response:{
			        // statusName:'code',
			        // msgName:'msg',
			        statusCode:2000,
			        // dataName:'data',
			        countName:'total',
			    }
			    ,request:{
			        // pageName:'page',
			        limitName: 'per_page'
			    }
			});
			
			table.render({
			    elem: '#banklist1'
			    ,id:'banklist1'
			    ,url: "/finances/basics/bank/index" //数据接口
			    ,cellMinWidth: 80
			    ,page: false //开启分页
			    ,method:'post'
			    ,limit:50
					,height: '260'//不含标签页,不含日报，月报，年报的高度
					// ,height: 'full-290' //含有tab标签页的高度
					// ,height: 'full-280' //含有日报，月报，年报的高度
			    ,initSort: {
			        field: 'fd_account_id' //排序字段，对应 cols 设定的各字段名
			        ,type: 'asc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
			    }
			    ,sort:true
			    ,cols: [[ //表头
			        {
								fixed: 'left',
			          type: 'numbers',
								title:'序号',
								width:80
			        }
			        ,{field: 'fd_account_companytype', title: '业务', align:'center'}
			        ,{field: 'fd_account_username', title: '金额', align:'center'}
			    ]]
			    ,done:function () {
			        $("[data-field='fd_account_id']").css('display','none');
			    }
			    ,response:{
			        // statusName:'code',
			        // msgName:'msg',
			        statusCode:2000,
			        // dataName:'data',
			        countName:'total',
			    }
			    ,request:{
			        // pageName:'page',
			        limitName: 'per_page'
			    }
			});
			
		});
	})
	
	function finSysMainBoxDate(){
	  var date=new Date();
	  var year=date.getFullYear();
	  var month=date.getMonth()+1;
	  var day=date.getDate();
		if(month<10){
			month = '0'+month;
		}
		if(day<10){
			day = '0'+day;
		}
	  document.getElementById("dateFixBox").innerHTML=year+"-"+month+"-"+day;
	}