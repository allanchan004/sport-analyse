$(document).ready(function () {
    if (typeof setMouseTips == "function") setMouseTips('i.layui-icon');
});

layui.use(['table','form','util'], function () {
    var form = layui.form
        ,table = layui.table //表格
        ,$ = layui.$
        ,util = layui.util
    ;

    util.fixbar({  //返回顶部
        top: true
        ,css: {right: 15, bottom: 35}
        ,bgcolor: '#393D49'
        ,click: function(type){

            // if(type === 'top'){
            //     layer.msg('返回顶部')
            // }

        }
    });

});


if(typeof getColor != "function") {
    getColor = function (score, is_full) {
        is_full = typeof is_full == "undefined" ? 0 : is_full;
        var color = '';
        switch (score) {
            case 0:
                color = 'score-red';
                break;
            case 1:
                color = 'score-blue';
                if (is_full) color = 'score-red';
                break;
            case 2:
                color = 'score-green';
                if (is_full) color = 'score-red';
                break;
            default:
                color = 'score-green';
                break;
        }
        return color;
    }
}


if(typeof setMouseTips != "function"){
    /**
     * 设置鼠标提示
     * @param selector
     */
    setMouseTips = function(selector){
        $(document).on('mouseenter', selector, function () {
            var obj = this;
            var msg = $(obj).attr('data-tips');
            if(typeof msg == "undefined" || msg == null || msg == '') return false;
            layer.tips(msg, obj, {
                tipsMore: true,
                time: 0,
                tips: 3
            });

        }).on('mouseleave', selector, function () {
            layer.closeAll();
        });
    }
}