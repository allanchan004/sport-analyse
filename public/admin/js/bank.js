///银行相关函数

/**
 * 获取银行名称列表
 * @param pay_type
 * @param form
 */
getBankNameList = function(bank_name,obj){
    layui.use(['laytpl','form'], function () {

        if( typeof obj != 'object' || obj == null || obj == undefined) obj = $('#payee_bank');
        var loading;
        var url = '/finances/basics/bank/nameList';
        $.ajax({
            type:       'post',
            data:       {per_page:0},
            dataType:   'json',
            url:url,
            beforeSend: function () {
                if(typeof layer == 'object') loading = layer.load(0, {shade: false});
            },
            success:    function (result) {
                if(loading) layer.close(loading);
                var html = '';
                html += '<option value=""></option>';
                for(var i in result.data){
                    var selected = '';if( result.data[i].fd_bank_name == bank_name ) selected = 'selected="selected"';
                    html += '<option value="'+result.data[i].fd_bank_name+'" ' + selected + '>'+result.data[i].fd_bank_name+'</option>';
                }
                obj.html(html);
                layui.form.render('select');
            }
        });

    });
}
