/**
 * 搜索列表页
 * @author: Allan
 */
// 更多搜索选项
var moreBtnVal = false;
$('.more-btn').on('click',function(){
    if(!moreBtnVal){
        $('.search-msg').eq(7).nextAll().removeClass('search-msg-hidden');
        $('.layui-card-more').css('height','auto');
        $(this).css({'transform':'rotate(180deg)'});
    }else{
        $('.search-msg').eq(7).nextAll().addClass('search-msg-hidden');
        $('.layui-card-more').css('height','124px');
        $(this).css({'transform':'rotate(0deg)'});

    }
    moreBtnVal = !moreBtnVal;
})

// 是否显示更多搜索选项,更多里面的元素都需要加类“.search-msg”
function isShowMore(){
    var searchMsgNum = $('.layui-card-more .search-msg').size();
    if(searchMsgNum > 8){
        $('.more-btn').show();
        $('.search-msg').eq(7).nextAll().addClass('search-msg-hidden');
        $('.layui-card-more').css({'height':'124px','padding-bottom': '23px'});
    }else{
        $('.more-btn').hide();
        $('.layui-card-more').css('height','auto');
    }
}

$(document).ready(function () {
    // 初始化
    isShowMore();
})