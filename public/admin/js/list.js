layui.use(['laypage','table','form','laydate','soulTable'], function () {
    var form = layui.form
        ,laypage = layui.laypage //分页
        ,table = layui.table //表格
        ,laydate = layui.laydate//日期
        ,$ = layui.$
    ;
    if(typeof getList == 'function') getList();
    form.on('submit(submit)',function(data){
        table.reload('list',{
            where:data.field,
            page: {
                curr: 1 //重新从第 1 页开始
            },
        });
        return false;   // 阻止表单跳转
    });
});

//回车进行搜索
$(document).keydown(function(event){
    if(event.keyCode==13){
        $('button[data-type="search"]').click();
    }
});
