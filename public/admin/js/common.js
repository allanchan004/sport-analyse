/**
 * 调用ERP的JS函数
 * @author:Gover_chan
 */
let erp = {
    /**
     * 回调方法收集器
     */
    callback: {},
    /**
     * 触发ERP系统的JS函数
     * @param functionName     调用的方法名
     * @param paramsArr        调用方法的参数，以数组的形式传入
     * @param apply            使用ERP哪个对象调用，默认window
     * @param url              非ERP系统下，不执行ERP的函数，进行跳转的链接
     * @param cbFlag           回调标记,建议存dgdata调用的方法名
     */
    trigger : function (functionName, paramsArr, apply, url, cbFlag) {
        var url       = url || '';
        var paramsArr = paramsArr || [];
        var apply     = apply || 'window';
        var cbFlag    = cbFlag || null;

        if (top.location != self.location) {
            window.parent.postMessage({'function': functionName, 'params': paramsArr, 'apply': apply, 'cbFlag': cbFlag}, '*');
        } else {
            if (url !== '') window.open(url, '_blank');
        }
    },

    /**
     * 监听ERp传送过来的数据
     * @param cbFlag            回调标记
     * @param callback          回调函数
     */
    addListen    : function (cbFlag, callback) {
        this.callback[cbFlag] = callback;
    },
    /**
     * 启动ERP监听
     */
    listen       : function () {
        let _this = this;
        window.addEventListener('message', function (e) {
            let jsonData = e.data;
            let cbFlag   = jsonData.cbFlag || null;
            //TODO 判断erp.callback[cbFlag]是否是一个方法
            _this.callback[cbFlag](jsonData.data);
        }, false);
    },
    /**
     * 在ERP的新的选项卡打开
     * @param url
     * @param title
     * @param rel     表单提交成功有需要在ERP关闭的使用form传入
     * @param erpPage 是否ERP系统的页面
     */
    navTab       : function (url, title, rel, erpPage = false) {
        var rel = rel || '_blank';
        this.trigger('erpNavTab', [url, title, rel, erpPage], 'window', url);
    },
    /**
     * 在ERP的弹窗打开
     * @param url
     * @param title
     * @param rel
     */
    open         : function (url, title, rel) {
        var rel = rel || 'dgData_flag';

        this.trigger('$.pdialog.open', [url, rel, title, {
            max      : false,
            mask     : true,
            mixable  : true,
            minable  : true,
            resizable: true,
            drawable : true,
            fresh    : true
        }], 'window', url);
    },
    /**
     * 在ERP中获取地区筛选的数据
     * @param val
     * @param level
     * @param type
     */
    changeArea   : function (val, level, type) {
        this.trigger('erpChangeArea', [val, level, type], 'window', '', 'changeArea');
    },
    /**
     * 在ERP中获取地区筛选的数据
     * @param pid
     * @param type
     */
    changeOrganic: function (pid, type) {
        this.trigger('erpChangeOrganic', [pid, type], 'window', '', 'changeOrganic');
    }
}

/**
 * 日期范围控件使用说明：
 * 1 添加class = 'date_input' ，
 * 2 添加data-type为时间选择器的类型，默认值为：date; 参考值：year、month、date、time、datetime
 * 3、name的名字带有begin_前缀和end_前缀作为一对，则为范围选择器
 * @type {{init}}
 * @author:ZZF
 */
let dgdata_input = (function ($) {
    //初始化日期控件
    function date() {
        let date_input_el = $('.date_input');
        let timeTipList   = ['start', 'begin'];
        let startTip      = '';
        date_input_el.each(function (i, el) {
            let start_el  = $(el);
            let startProp = start_el.attr("name");
            let dataType  = start_el.data("type") || '';

            for (let i = 0; i < timeTipList.length; i++) {
                if (startProp.indexOf(timeTipList[i]) !== -1) {
                    startTip = timeTipList[i];
                }
            }
            //初始化范围时间控件
            let end_el = ''
            if (startTip != '') {
                endProp = startProp.replace(startTip, 'end');
                if (endProp) {
                    end_el = $("input[name=" + endProp + "]");
                }
                if (end_el.length == 1) {
                    start_el.attr('id', startProp).attr('readonly', 'readonly');
                    end_el.attr('id', endProp).attr('readonly', 'readonly');

                    let start_date = laydate.render({
                        elem: '#' + startProp, //指定元素
                        type: dataType || checkType(start_el.val()),
                        max : new Date().getTime(),
                        done: function (value, date) {
                            end_date.config.min = {
                                year : date.year,
                                month: date.month - 1,
                                date : date.date,
                            }
                        }
                    })
                    let end_date   = laydate.render({
                        elem: '#' + endProp, //指定元素
                        type: dataType || checkType(end_el.val()),
                        max : new Date().getTime(),
                        done: function (value, date) {
                            start_date.config.max = {
                                year : date.year,
                                month: date.month - 1,
                                date : date.date,
                            }
                        }
                    })
                }
            }

            //初始化单个时间控件
            if (end_el.length != 1 && startProp != "") {
                start_el.attr('id', startProp).attr('readonly', 'readonly');
                laydate.render({
                    elem: '#' + startProp, //指定元素
                    type: dataType || checkType(start_el.val()),
                    max : new Date().getTime()
                })
            }

        });
    }

    //无data-type属性时检测初始值的类型
    function checkType(val) {
        let typeStr   = 'date';  //默认为date;
        let yearType  = /^\d{4}$/; //年
        let monthType = /^\d{4}-\d{2}$/;  //月
        let time      = /^\d{2}:\d{2}:\d{2}$/;  //时间
        let datetime  = /^\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}$/;  //日期加时间
        if (yearType.test(val)) {
            typeStr = 'year';
        } else if (monthType.test(val)) {
            typeStr = 'month';
        } else if (time.test(val)) {
            typeStr = 'time';
        } else if (datetime.test(val)) {
            typeStr = 'datetime';
        }
        return typeStr;
    }

    return {
        init: function (func) {
            $(function () {
                let f = eval(func);
                typeof f === 'function' && f();
            })
        }
    };
})(jQuery);
dgdata_input.init('date');


/**
 * 其它初始化
 * @author:Gover_chan
 */
$(function () {
    let body_el = $('body');

    //便捷跳转:添加data-href属性，便可跳转
    body_el.on('click', '[data-href]', function (e) {
        e.stopPropagation();  //阻止冒泡;
        let val = $(this).data('href');
        if (!val) return;
        document.location.href = val;
    });

    //便捷打开erp的navTab:添加data-href属性，便可跳转
    var eX = 0;
    var eY = 0;
    var lastX = 0;
    var lastY = 0;
    body_el.on('mousedown', '[data-erp-navtab]', function (e) {
        e.stopPropagation();  //阻止冒泡;
        eX = e.pageX;
        eY = e.pageY;
        let url      = $(this).data('erp-navtab');
        let navTitle = $(this).data('erp-title') || $(this).html() + '-页面';
        if (!url) return;

        let flag = escape(url);
        let rel  = $(this).data('erp-rel') || flag;
        document.onmouseup = function(ev){
            lastX = ev.pageX;
            lastY = ev.pageY;
            if(Math.abs(lastX - eX) > 5 || Math.abs(lastY - eY) > 5 || (/^\d+(\.?\d+)?$/.test(parseFloat(ev.target.innerHTML)) && parseFloat(ev.target.innerHTML) == 0)){
                // something...
            }else if(ev.button == 0 || ev.button == 1){
                erp.navTab(url, navTitle, rel);
            }
            document.onmouseup = null;
            eX = 0;
            eY = 0;
            lastX = 0;
            lastY = 0;
        };


    });

    /**
     * 给有title属性的元素，添加data-toggle, 无需要手动添加
     */
    $("[title]").attr("data-toggle",'tooltip');
    $("[title]").attr("data-placement",'top auto');

    /**
     * 表格排序:添加属性data-order,值为字段名
     * <th data-order="tel">手机号</th>
     */
    let orderName = getQueryString("_order");
    let sortType = getQueryString("_sort");
    $("[data-order]").each(function(i,n){
        $(this).attr('data-sort',"");
    })
    $("[data-order="+orderName+"]").attr("data-sort",sortType);
    body_el.on('click', '[data-order]', function () {
        let _this = $(this);
        let clientXStr = getDateIndex($(this))+","+$(this)[0].getBoundingClientRect().left;
        let order = _this.data('order');
        let sort  = _this.data('sort');
        if (sort) {
            sort = sort === 'desc' ? 'asc' : 'desc';
            _this.data('sort', sort);
        } else {
            sort = 'desc';
            _this.data('sort', sort);
        }

        if (!order) return layer.msg('排序字段没有赋值');

        let url = document.location.href;
        url     = changeUrlArg(url, '_order', order);
        url     = changeUrlArg(url, '_sort', sort);
        url     = changeUrlArg(url, 'clientXStr', clientXStr);

        document.location.href = url;
    });

    /**
     * zTitle扩展title，鼠标划过显示表格
     * 添加属性:  data-ztitle(数据), 必须设置,值为二维数组
     *
     * 示例1: <td data-ztitle='[["姓名","年龄","性别"],["abc","18","男"],["efg","18","女"]]'></td>
     *
     * 示例2，存在多行表头: <td data-ztitle='[{"年龄":"2","性别":"2"},["年龄1","年龄2","性别1","性别2"],["18","20","男","女"]]'></td>
     * 上面示例2中，存在colspan的行要写成对象，键有标题，值为colspan的值
     * @author: zefeng.zheng
     */
    body_el.on('mouseover', "[data-ztitle]",function(e){
        var newTitle = $(this).data("ztitle");
        $('body').append('<div class="ztitleWrap"></div>');
        if(typeof newTitle == 'object'){
            $(".ztitleWrap").css("maxWidth",'none');
            var iType = 'table';  //目前写死table,之后有需求再写成动态
            switch(iType) {
                case 'table':
                    zTitle.table(newTitle);
                    break;
            }
        }else{
            $('.ztitleWrap').append(newTitle);
        }
        setZtitleXY(e);
        $('.ztitleWrap').css({'visibility': "visible"})
    })
    body_el.on('mousemove', "[data-ztitle]",function(e){
        setZtitleXY(e);
    })
    body_el.on('mouseout', "[data-ztitle]",function(e){
        $('.ztitleWrap').remove();
    })
    // zTitle，根据类型生成对应DOM元素
    var zTitle = {
        // 表格类型
        table: function(tList){
            var tableDom = "<table><thead><tr>";
            tableDom += "</tr></thead><tbody>";
            if(tList){
                tList.forEach(function(item){
                    tableDom += "<tr>";
                    if(item instanceof Array){
                        item.forEach(function(i){
                            tableDom += "<td>"+i+"</td>";
                        })
                    }else{
                        for(var i in item){
                            tableDom += "<td style='text-align:center;' colspan="+item[i]+">"+i+"</td>";
                        }
                    }

                    tableDom += "<tr>";
                })
                if(tList.length == 1){
                    var tdLen = tList[0].length;
                    tableDom += "<tr>";
                    tableDom += "<td align='center' colspan='"+tdLen+"'>暂无数据</td>";
                    tableDom += "<tr>";
                }
            }
            tableDom += "</tbody></table>";
            $('.ztitleWrap').append(tableDom);
        }
    }
    // 设置ztitle浮窗的位置
    function setZtitleXY(e){
        var winW = $(window).width();
        var winH = $(window).height();
        var eW = $(".ztitleWrap")[0].offsetWidth;
        var eH = $(".ztitleWrap")[0].offsetHeight;
        var leftX = eW + e.pageX + 10 > winW ? e.pageX - eW + 10 : e.pageX + 10;
        var topY = eH + e.pageY + 10 > winH ? e.pageY - eH -10 : e.pageY + 10;
        if(topY < 0) topY = 0;
        $('.ztitleWrap').css({'left': (leftX + 'px'),'top': (topY + 'px')});
    }

    /**
     * 在当前页面做排列，不做请求
     *
     */
    body_el.on('click', '[data-sort-local]', function () {
        let _this = $(this);
        if($(".trueTable").length>0){
            $("[data-sort-local]").not(_this).attr('data-sort-local','123456');
        }else{
            _this.parents("table").find("[data-sort-local]").not(_this).attr('data-sort-local','123456');
        }

        let sort  = _this.attr('data-sort-local');
        if (sort) {
            sort = sort === 'desc' ? 'asc' : 'desc';
            _this.attr('data-sort-local', sort);
        } else {
            sort = 'desc';
            _this.attr('data-sort-local', sort);
        }

        let sortIndex = getDateIndex(_this);

        if($("#pageWrap").length > 0){   // 有前端分页
            zTable.entry.sort(sortIndex, sort);
        }else{
            if($(".trueTable").length>0){
                var oTable = document.getElementsByClassName("trueTable")[0];//获取以class为classnametable对象
                sortTable(oTable,sortIndex,sort)
            }else{
                sortTable(_this.parents("table")[0],sortIndex,sort)
            }
        }
    });
    function sortTable(tableObj,rowNum,sortType){
        var oTbody = tableObj.tBodies[0];//获取第一个tBody
        var oRows = oTbody.rows;//获取tBody的所有的行
        var aTRs = new Array;//新建一个数组
        for (var i=0;i < oRows.length ;i++ ){
            aTRs[i] = oRows[i];//循环将所有行换到新的数组
        }
        aTRs.sort( function(oTR1,oTR2){
            var s1 = inAadvance(oTR1.cells[rowNum].firstChild.nodeValue);//比较行的内容的值
            var s2 = inAadvance(oTR2.cells[rowNum].firstChild.nodeValue);//比较行的内容的值
            if(sortType=='asc'){
                return s1-s2;//进行比较
            }else{
                return s2-s1;//进行比较
            }

        });//对数组进行比较,是通过新的数组的行比较
        var otemp = document.createDocumentFragment();//创建文档碎片
        for (var i = 0;i<aTRs.length ;i++ ){
            if($("[data-sort-number]").length>0) {
                aTRs[i].cells[0].innerHTML=i+1;
            }
            otemp.appendChild(aTRs[i]);//分别将每行加入到文档碎片中
        }
        oTbody.appendChild(otemp);//将文档碎片添加到文档中

        if(document.getElementsByClassName('trueTable')[1]){   //有固定列的情况
            let fixedTable = document.getElementsByClassName('trueTable')[1];
            fixedTable.removeChild(fixedTable.tBodies[0]);
            fixedTable.appendChild(oTbody.cloneNode(true));
        }

        if($(".trueTable").length > 0){
            $(".tableWrap").scrollTop(0);
        }

    }
    // 对数据预先处理
    function inAadvance(value){

        var isTimeReg = /^\d+[-|/]\d+([-|/]\d+)?$/;  //时间正则
        if(isTimeReg.test(value)){//字符串为时间格式就转为时间对象
            return new Date(value);
        }else{
            return parseFloat(value) || 0;
        }
    }



    /**
     * 当前页搜索
     */
    body_el.on("submit", "[data-search-local]", function(e){
        e.preventDefault();   //阻止提交
        var _this = $(this);
        var searchList = [];  //查询列表
        var subparams = _this.serializeArray();  //参数和值组成的json
        subparams.forEach(function(item){
            var dataIndex = getDateIndex($("[data-search='" + item.name + "']").eq(0));
            item.value = item.value.trim();
            if(item.value !='' && dataIndex != -1){
                item.index = dataIndex;
                searchList.push(item);
            }
        })
        if($("#pageWrap").length>0){  //有是前端分页
            zTable.entry.search(searchList);

        }else{   //无分页情况
            var tBody = $(".trueTable")[0].tBodies[0];
            var oRows;  //表格行集合
            if(window.fullDataRows){   //完整的数据列表
                oRows = window.fullDataRows.rows;
            }else{
                oRows = tBody.rows;
                window.fullDataRows = tBody.cloneNode(true);   //完整的数据列表保存到全局window下

            }
            var dataList = document.createDocumentFragment();    //匹配的数据列表
            Array.prototype.forEach.call(oRows,function(trItem){//数据验证
                var tdList = trItem.cells;  //该行的单元格集合
                var isMatch = true;
                for(var i=0;i<searchList.length;i++){
                    var item = searchList[i];
                    var numReg = /^\d+$/;  //数字字符串
                    var tdContent = tdList[item.index].textContent;
                    if(numReg.test(tdContent)){  //数字
                        if(tdContent.indexOf(item.value) != 0 ){
                            isMatch = false;
                            break;
                        }
                    }else{   //非数字
                        if(tdContent.indexOf(item.value) == -1 ){
                            isMatch = false;
                            break;
                        }
                    }
                }
                if(isMatch){  //数据完全匹配，要展示
                    dataList.appendChild(trItem.cloneNode(true));
                }
            })
            if(dataList.childNodes.length == 0){   //没有数据匹配时
                var trDom = document.createElement("tr");
                var tdDom = document.createElement("td");
                tdDom.setAttribute('colspan',60);
                tdDom.innerHTML = '查询无果';
                tdDom.align = 'center';
                trDom.appendChild(tdDom);
                dataList.appendChild(trDom);
            }
            $(".trueTable tbody").empty().append(dataList);  //插入数据
            dataList = null;
        }

    })
    //清空查询
    $("[data-search-local] button").eq(1).attr("onclick",null).click(function(){
        if($("#pageWrap").length>0){  //有分页，并且是前端分页
            zTable.entry.reset();
        }else if(window.fullDataRows){
            var dataList = document.createDocumentFragment();
            var oRows =  window.fullDataRows.rows;
            Array.prototype.forEach.call(oRows, function(trItem){
                dataList.appendChild(trItem.cloneNode(true));
            })
            $(".trueTable tbody").empty().append(dataList);
            window.fullDataRows = null;
        }

        $(this.form).serializeArray().forEach(function(item){  //清空form表单值
            $("["+ item.name +"]").val("");
        })
    });
});

/**
 * 替换或者追加URL参数
 * @param url
 * @param arg
 * @param val
 * @returns {string}
 */
function changeUrlArg(url, arg, val) {
    let pattern     = arg + '=([^&]*)';
    let replaceText = arg + '=' + val;
    return url.match(pattern) ? url.replace(eval('/(' + arg + '=)([^&]*)/gi'), replaceText) : (url.match('[\?]') ? url + '&' + replaceText : url + '?' + replaceText);
}

/**
 * 根据变量名获取get参数匹配值
 * @param name
 * @returns {*}
 */
function getQueryString(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    let r   = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return false;
}

//对字符串进行加密
function compileStr(code) {
    var c = String.fromCharCode(code.charCodeAt(0) + code.length);
    for (var i = 1; i < code.length; i++) {
        c += String.fromCharCode(code.charCodeAt(i) + code.charCodeAt(i - 1));
    }
    return escape(c);
}

//字符串进行解密
function uncompileStr(code) {
    code  = unescape(code);
    var c = String.fromCharCode(code.charCodeAt(0) - code.length);
    for (var i = 1; i < code.length; i++) {
        c += String.fromCharCode(code.charCodeAt(i) - c.charCodeAt(i - 1));
    }
    return c;
}


/**
 * 设置select默认值
 * @param name
 * @param val
 */
function setDefaultSelect(name, val) {
    $('select[name=' + name + ']').find('option[value=' + val + ']').attr('selected', true)
}


/**
 * 根据表头的某列，求出在数据项对应第几列
 * @param itemDom 表头某列的节点(jq对象)
 */
function getDateIndex(itemDom){
    let sortIndex = 0;

    let _this = itemDom;  //这一顶的dom节点
    let indexClick = _this.index();   //这一顶在该列的下标
    parentIndex = _this.parent().index();  //这一项的列在该tbody的下标

    for(var i=parentIndex;i>=0;i--){
        let parentTr = _this.parents("thead").find("tr").eq(i);
        if(i == parentIndex){
            let num = 0;
            for(var j=0;j<parentTr.find("th").length;j++){
                if(j > indexClick) break;
                num += parseInt(parentTr.find("th").eq(j).attr("colspan")) || 1;
            }
            sortIndex = num;
        }else{
            let num = 0;
            for(var j=0;j<parentTr.find("th").length;j++){
                if(parentTr.find("th").eq(j).attr("colspan")){
                    num += parseInt(parentTr.find("th").eq(j).attr("colspan"));
                    if(num >= sortIndex){
                        for(var z=0;z<j;z++){
                            if(!parentTr.find("th").eq(z).attr("colspan")){
                                sortIndex ++;
                            }
                        }
                        break;
                    }
                }
            }
        }
    }
    return sortIndex - 1;

}

/**
 * 获取表单JSON格式数据
 * @param id
 */
function getFormJson(id) {
    var d = {};
    var t = $('#'+id).serializeArray();
    $.each(t, function() {
        d[this.name] = this.value;
    });
    return d;
}

/*添加、编辑区、搜索区所属组织*/
function selectOrganic(obj, typeAction=0) {
    var _url=$(obj).parent().parent().find('.ajaxGetOrganic').val();
    if(_url=="" || _url==undefined){
        layer.msg('URL参数有误');
        return false;
    }
    $(obj).parent().nextAll().remove();
    var pid = obj.value;
    if (pid=="" || pid==0) {
        return false;
    }
    var data = {pid:pid};
    if ( typeAction>0 ) {
        data.type='staffer';
    }
    $.get(_url, data, function(res) {
        if (res.code==0) {
            console.log(res);
            // if (res.data.info!='') {
            //     var data=res.data.info;
            //     var selectName=res.data.organic_name;
            //     var selectStr='';
            //     selectStr+="<select class='select_organic' onchange='changeOrganic(this)' name='pid[]' style='width: 150px;'><option value="+""+">"+selectName+"</option>";
            //     for(var i=0;i<data.length;i++){
            //         selectStr+="<option value="+data[i]['id']+">"+data[i]['name']+"</option>";
            //     }
            //     selectStr+="</select> ";
            //     $(obj).parent().append(selectStr);
            // }
        } else {
            layer.msg(res.msg);
        }
    });
}