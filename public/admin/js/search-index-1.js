/**
 * 搜索列表页
 * @author: Allan
 */
var isShowMore = false,isFixed = true;
$('.more-btn').on('click',function(){
    toggleSearch();
});

$('.fixed-btn').on('click',function(){
    isFixed = !isFixed;
    if( isFixed ){
        $('.fixed-btn').css({'transform':'rotate(90deg)'});
    }else{
        $('.fixed-btn').css({'transform':'rotate(0deg)'});
    }
});
/* 暂时 关闭此功能 */
/*$('.search-div').hover(function () {
    isShowMore = true;
    toggleSearch();
},function () {
    if( !isFixed ){
        isShowMore = false;
        toggleSearch();
    }
})*/

toggleSearch = function(){
    if(isShowMore){
        $('.search-msg-show').removeClass('search-msg-hidden');
        $('.layui-card-more').css('height','auto');
        $('.more-btn').css({'transform':'rotate(180deg)'});
        //暂时不展示
        // $('.fixed-btn').show();
    }else{
        $('.search-msg-show').addClass('search-msg-hidden');
        var height = $('.search-div').attr('default-height');
        if( height == undefined ) height = 60;
        $('.layui-card-more').css('height',height + 'px');
        $('.more-btn').css({'transform':'rotate(0deg)'});

        $('.fixed-btn').hide();
    }
    isShowMore = !isShowMore;
}

$(document).ready(function () {
    $('body').css('overflow','hidden');
    // 初始化
    $('#department_select').addClass('search-msg-show');
    $('#organize_select').addClass('search-msg-show');

    $('.more-btn').show();
    $('.more-btn').css({'transform':'rotate(180deg)'});

    $('.fixed-btn').hide();
    $('.fixed-btn').css({'transform':'rotate(90deg)'});

    toggleSearch();
});