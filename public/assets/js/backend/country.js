define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'country/index' + location.search,
                    add_url: 'country/add',
                    edit_url: 'country/edit',
                    del_url: 'country/del',
                    multi_url: 'country/multi',
                    import_url: 'country/import',
                    table: 'country',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                sortOrder:'ASC',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'name', title: __('Name'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'oceania_name', title: __('所属大洲'), operate: 'LIKE'},
                        {field: 'ico', title: __('Ico'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.image,searchable:false},
                        {field: 'status', title: __('Status'), searchList: {"1":__('启用'),"0":__('禁用')}, formatter: Table.api.formatter.toggle},
                        {field: 'update_user', title: __('Update_user'), operate: 'LIKE'},
                        {field: 'updated_at', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
