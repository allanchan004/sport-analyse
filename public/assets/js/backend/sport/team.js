define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    Fast.config.openArea = ['90%','90%'];
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'sport/team/index' + location.search,
                    add_url: 'sport/team/add',
                    edit_url: 'sport/team/edit',
                    del_url: 'sport/team/del',
                    multi_url: 'sport/team/multi',
                    import_url: 'sport/team/import',
                    table: 'sport_team',
                }
            });

            var table = $("#table");
            //顶部搜索栏
            table.on('post-common-search.bs.table', function (event, table) {
                var form = $("form", table.$commonsearch);
                $('input[name="club_id"]', form).addClass("selectpage")
                    .data("source", "sport/club/index")
                    .data("primaryKey", "id")
                    .data("field", "name")
                    .data("multiple", true)
                    .data("orderBy", "id ASC")
                    .data("params",function(obj){
                        return {custom:{}};
                    });

                Form.events.cxselect(form);
                Form.events.selectpage(form);

            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('序号'),searchable:false},
                        {field: 'club.name', title: __('Club_id'),searchable:false},
                        {field: 'club_id', title: __('Club_id'), operate: 'IN',visible:false},
                        {field: 'name', title: __('Name'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'full_name', title: __('Full_name'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'ico', title: __('Ico'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.image,searchable:false},

                        {field: 'update_user', title: __('Update_user'), operate: 'LIKE'},
                        {field: 'updated_at', title: __('Updated_at'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
