define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    Fast.config.openArea = ['100%','100%'];
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'sport/score/index' + location.search,
                    add_url: 'sport/score/add',
                    edit_url: 'sport/score/edit',
                    del_url: 'sport/score/del',
                    multi_url: 'sport/score/multi',
                    import_url: 'sport/score/import',
                    table: 'sport_score',
                }
            });

            var table = $("#table");

            //顶部搜索栏
            table.on('post-common-search.bs.table', function (event, table) {
                var form = $("form", table.$commonsearch);
                $('input[name="country_id"]', form).addClass("selectpage")
                    .data("source", "country/index")
                    .data("primaryKey", "id")
                    .data("field", "name")
                    .data("multiple", true)
                    .data("orderBy", "id ASC")
                    .data("params",function(obj){
                        return {custom:{}};
                    });

                Form.events.cxselect(form);
                Form.events.selectpage(form);

            });

            //顶部搜索栏
            table.on('post-common-search.bs.table', function (event, table) {
                var form = $("form", table.$commonsearch);
                $('input[name="club_id"]', form).addClass("selectpage")
                    .data("source", "sport/club/index")
                    .data("primaryKey", "id")
                    .data("field", "name")
                    .data("multiple", true)
                    .data("orderBy", "id ASC")
                    .data("params",function(obj){
                        return {custom:{}};
                    });

                Form.events.cxselect(form);
                Form.events.selectpage(form);

            });

            //顶部搜索栏
            table.on('post-common-search.bs.table', function (event, table) {
                var form = $("form", table.$commonsearch);
                $('input[name="main_team_id"]', form).addClass("selectpage")
                    .data("source", "sport/team/index")
                    .data("primaryKey", "id")
                    .data("field", "name")
                    .data("multiple", true)
                    .data("orderBy", "id ASC")
                    .data("params",function(obj){
                        return {custom:{}};
                    });

                Form.events.cxselect(form);
                Form.events.selectpage(form);

            });
            //顶部搜索栏
            table.on('post-common-search.bs.table', function (event, table) {
                var form = $("form", table.$commonsearch);
                $('input[name="visiting_team_id"]', form).addClass("selectpage")
                    .data("source", "sport/team/index")
                    .data("primaryKey", "id")
                    .data("field", "name")
                    .data("multiple", true)
                    .data("orderBy", "id ASC")
                    .data("params",function(obj){
                        return {custom:{}};
                    });

                Form.events.cxselect(form);
                Form.events.selectpage(form);

            });



            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                pageSize: 100,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('序号'),searchable:false},

                        {field: 'country_id', title: __('Country_name'),operate: 'IN',visible:false},
                        {field: 'club_id', title: __('Club_name'),operate: 'IN',visible:false},

                        {field: 'country.name', title: __('Country_name'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content,searchable:false},
                        {field: 'club.name', title: __('Club_name'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content,searchable:false},
                        {field: 'match_time', title: __('Match_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},



                        {field: 'main_team_id', title: __('Main_team'),operate: 'IN',visible:false},
                        {field: 'visiting_team_id', title: __('Visiting_team'),operate: 'IN',visible:false},

                        {field: 'main_team', title: __('Main_team'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content,searchable:false},
                        {field: 'visiting_team', title: __('Visiting_team'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content,searchable:false},

                        {field: 'first_half', title: __('上半场比分'),formatter:function (value, row, index)  {
                                return row.first_half_m_score + ':' + row.first_half_v_score;
                            },searchable:false},
                        {field: 'second_half', title: __('下半场比分'),formatter:function (value, row, index)  {
                                return row.second_half_m_score + ':' + row.second_half_v_score;
                            },searchable:false},
                        {field: 'full_half', title: __('全场比分'),formatter:function (value, row, index)  {
                                return row.full_m_score + ':' + row.full_v_score;
                            },searchable:false},


                        {field: 'match_date', title: __('Match_date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},

                        {field: 'ori_match_time', title: __('Ori_match_time'), operate: 'LIKE',searchable:false},
                        {field: 'club.version', title: __('Club.version'), operate: 'LIKE'},

                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
