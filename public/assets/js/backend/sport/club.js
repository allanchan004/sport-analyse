define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    Fast.config.openArea = ['90%','90%'];
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'sport/club/index' + location.search,
                    add_url: 'sport/club/add',
                    edit_url: 'sport/club/edit',
                    del_url: 'sport/club/del',
                    multi_url: 'sport/club/multi',
                    import_url: 'sport/club/import',
                    table: 'sport_club',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                sortOrder:'ASC',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('序号'),searchable:false},
                        {field: 'type_text', title: __('Type_name'),searchable:false},
                        {field: 'version', title: __('Version'), operate: 'LIKE'},
                        {field: 'country.name', title: __('Country_id')},
                        {field: 'name', title: __('Name'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'full_name', title: __('Full_name'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'ico', title: __('Ico'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.image,searchable:false},
                        {field: 'update_user', title: __('Update_user'), operate: 'LIKE',searchable:false},
                        {field: 'updated_at', title: __('Updated_at'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
