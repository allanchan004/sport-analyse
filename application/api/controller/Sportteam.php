<?php

namespace app\api\controller;

use app\common\controller\Api;

/**
 * 示例接口
 */
class Sportteam extends Api
{
    use \app\common\traits\SportTeam;

    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['*'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];


    public function index(){
        $this->success('', self::getList(input()));
    }

}
