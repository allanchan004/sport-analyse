<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Log;

/**
 * 示例接口
 */
class Matchnotice extends Api
{
    use \app\common\traits\MatchNotice;

    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['*'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];


    public function index(){
        $this->success('', self::getList(input()));
    }


    /**
     * 批量添加
     */
    public function batchAdd(){

        $result = [
            'total'=>0,
            'success'=>0,
        ];

        try{
            $lines = explode("\n",input('lines'));
            $result = self::graspMatchs($lines);
        }catch(\Exception $e){
            Log::write($e->getMessage());
            Log::write($e->getTraceAsString());
            $this->error($e->getMessage(),null,$e->getCode());
        }

        $this->success('',$result);


    }

}
