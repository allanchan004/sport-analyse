<?php

namespace app\admin\model\sport;

use think\Model;
use traits\model\SoftDelete;


class Score extends Model
{

    use SoftDelete;

    // 表名
    protected $name = 'sport_score';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';

    // 定义时间戳字段名
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';
    protected $deleteTime = 'deleted_at';
    protected $dateFormat = 'Y-m-d H:i:s';


    // 追加属性
    protected $append = [
        'match_time_text'
    ];


    public function getMatchTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['match_time']) ? $data['match_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    protected function setMatchTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function country()
    {
        return $this->belongsTo('app\admin\model\Country', 'country_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function club()
    {
        return $this->belongsTo('Club', 'club_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
