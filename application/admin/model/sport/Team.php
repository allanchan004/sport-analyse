<?php

namespace app\admin\model\sport;

use think\Model;
use traits\model\SoftDelete;


class Team extends Model
{
    use SoftDelete;

    // 表名
    protected $name = 'sport_team';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';

    // 定义时间戳字段名
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';
    protected $deleteTime = 'deleted_at';
    protected $dateFormat = 'Y-m-d H:i:s';

    // 追加属性
    protected $append = [

    ];
    


    public function club()
    {
        return $this->belongsTo('Club', 'club_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
