<?php

return [
    'Oceania_type'   => '大洋洲：1 亚洲；2 欧洲；3 非洲；4 北美洲；5 南美洲；6 大洋洲；7 南极洲',
    'Oceania_name'   => '所属大洋洲',
    'Name'           => '名称',
    'Ico'            => '缩略图',
    'Status'         => '状态',
    'Update_user_id' => '更新人ID',
    'Update_user'    => '更新人名称',
    'Update_time'    => '更新时间',
    'Create_time'    => '创建时间',
    'Delete_time'    => '删除时间'
];
