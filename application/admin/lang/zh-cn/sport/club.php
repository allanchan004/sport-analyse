<?php

return [
    'Type'                   => '类型：1 俱乐部独家FIFA；2 国家队IM独家PES；',
    'Type_name'                   => '类型',
    'Version'                => '版本',
    'Country_id'             => '所属地区',
    'Name'                   => '名称',
    'Full_name'              => '全称',
    'Ico'                    => '缩略图',
    'Pics'                   => '多图片',
    'Create_user'            => '创建人',
    'Update_user'            => '更新人',
    'Updated_at'             => '更新时间',
    'Created_at'             => '创建时间',
    'Deleted_at'             => '删除时间',
    'Country.oceania_type'   => '大洋洲：1 亚洲；2 欧洲；3 非洲；4 北美洲；5 南美洲；6 大洋洲；7 南极洲',
    'Country.name'           => '名称',
    'Country.ico'            => '缩略图',
    'Country.status'         => '状态',
    'Country.update_user_id' => '更新人ID',
    'Country.update_user'    => '更新人名称',
    'Country.updated_at'     => '更新时间',
    'Country.created_at'     => '创建时间',
    'Country.deleted_at'     => '删除时间'
];
