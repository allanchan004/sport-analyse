<?php

return [
    'Club_id'          => '所属俱乐部',
    'Name'             => '球队名称',
    'Full_name'        => '球队全称',
    'Ico'              => '缩略图',
    'Pics'             => '多图片',
    'Create_user'      => '创建人',
    'Update_user'      => '更新人',
    'Updated_at'       => '更新时间',
    'Created_at'       => '创建时间',
    'Deleted_at'       => '删除时间',
    'Club.type'        => '类型：1 俱乐部独家FIFA；2 国家队IM独家PES；',
    'Club.version'     => '版本',
    'Club.country_id'  => '所属地区',
    'Club.name'        => '名称',
    'Club.full_name'   => '全称',
    'Club.ico'         => '缩略图',
    'Club.pics'        => '多图片',
    'Club.create_user' => '创建人',
    'Club.update_user' => '更新人',
    'Club.updated_at'  => '更新时间',
    'Club.created_at'  => '创建时间',
    'Club.deleted_at'  => '删除时间'
];
