<?php

namespace app\index\controller;

use app\common\controller\Frontend;


class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function index()
    {
        $this->view->assign('analyseUrl',url('/analyse/index'));
        $this->view->assign('analyseUrl2022',url('/analyse/index/2022'));
        $this->view->assign('matchNoticeUrl',url('/analyse/matchNotice'));

        return $this->view->fetch();
    }

}
