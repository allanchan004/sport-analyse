<?php

namespace app\index\controller;


use app\common\controller\Frontend;
use app\common\model\Country;
use app\common\model\sport\Club;
use app\common\utils\Functions;
use think\Log;

class Analyse extends Frontend
{
    use \app\common\traits\SportScore;

    public $title = '分析';


    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';


    /**
     * 初始化 数据
     * @throws
     */
    public function initOption($v = 2020){

        $this->view->assign('title',$this->title);

        $this->view->assign('indexUrl',url('/api/SportScore/index'));

        $this->view->assign('selectTeamUrl',url('/api/SportTeam/index'));
        $this->view->assign('selectClubUrl',url('/api/SportClub/index'));


        $this->view->assign('mnIndexUrl',url('/api/MatchNotice/index'));
        $this->view->assign('mnBatchAddUrl',url('/api/MatchNotice/batchAdd'));

        $this->view->assign('matchNoticeUrl',url('/Analyse/matchNotice'));
        $this->view->assign('batchAddUrl',url('/Analyse/batchAdd/' . $v));
        $this->view->assign('snycScoreUrl',url('/Analyse/snycScore'));
        $this->view->assign('snycMatchUrl',url('/Analyse/snycMatch'));

        $countries = collection(Country::all())->toArray();
        $this->view->assign('country_array', array_combine(array_column($countries, 'id'), array_column($countries, 'name')));

        $hourArr = [];
        for($i = 0;$i < 24;$i++) $hourArr[] = sprintf("%02d", $i);
        $minuteArr = [];
        for($i = 0;$i < 60;$i++) $minuteArr[] = sprintf("%02d", $i);

        $this->view->assign('hourArr',Functions::setSelectOption($hourArr));
        $this->view->assign('minuteArr',Functions::setSelectOption($minuteArr));

    }


    public function index($v = 2020)
    {
        if($this->request->isAjax()){
            $this->success(__('success'),null,self::getList(input()));
        }

        $type = input('type',Club::TYPE1);
        $this->title .= ' - ' . (new Club)->getTypeList()[$type] . ' ' . $v;

        $this->initOption($v);

        $countries = collection(Country::all())->toArray();
        $countryArr = array_combine(array_column($countries, 'id'), array_column($countries, 'name'));
        if($type == Club::TYPE2) $countryArr = (new Country)->getOceaniaTypeList();

        $this->view->assign('country_array', $countryArr);
        $this->view->assign('type', $type);
        $this->view->assign('v', $v);

        return view('analyse/index');
    }


    public function batchAdd($v = 2020){
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
        if($this->request->isAjax()){
            $result = [
                'total'=>0,
                'success'=>0,
            ];

            try{
                $lines = explode("\n",input('lines'));
                $result = self::graspScores($lines,$v);

            }catch(\Exception $e){
                Log::error($e->getMessage());
                Log::error($e->getTraceAsString());
                $this->error($e->getMessage(),null,$e->getCode());
            }

           $this->success(__('success'),null,$result);
        }
        $this->view->assign('v', $v);
        return view('analyse/batchAdd');
    }

    public function matchNotice()
    {
        if($this->request->isAjax()){
            $this->success('',null,\app\common\traits\MatchNotice::getList(input()));
        }
        $this->title = '比赛预告';

        $type = input('type',Club::TYPE1);

        $this->initOption();

        $countries = collection(Country::all())->toArray();
        $countryArr = array_combine(array_column($countries, 'id'), array_column($countries, 'name'));
        if($type == Club::TYPE2) $countryArr = (new Country)->getOceaniaTypeList();

        $this->view->assign('country_array', $countryArr);
        $this->view->assign('type', $type);

        return view('analyse/matchNotice');
    }

    public function matchNotice2()
    {
        if($this->request->isAjax()){
            return $this->matchNotice();
        }
        $this->title = '比赛预告';
        if( input('title') ) $this->title .= ' - '.input('title');


        $this->initOption();


        return view('Analyse/matchNotice2');
    }



    public function teamMatchDetail()
    {
        $this->title = '球队比赛详情';
        $this->initOption();
        $this->view->assign('name',input('name'));
        return view('analyse/teamMatchDetail');
    }

    public function detail(){
        $this->title  = '比赛详情';
        if( input('title') ) $this->title .= ' - '.input('title');

        $this->initOption();

        $matchUrl = url('/analyse/teamMatchDetail',['match_time'=>input('match_time'),'main_team_id'=>input('main_team_id'),'visiting_team_id'=>input('visiting_team_id'),'name'=>'交手历史']);
        $mainUrl = url('/analyse/teamMatchDetail',['match_time'=>input('match_time'),'team_id'=>input('main_team_id'),'name'=>'主队近日比赛']);
        $visitingUrl = url('/analyse/teamMatchDetail',['match_time'=>input('match_time'),'team_id'=>input('visiting_team_id'),'name'=>'客队近日比赛']);

        $this->view->assign('matchUrl',$matchUrl);
        $this->view->assign('mainUrl',$mainUrl);
        $this->view->assign('visitingUrl',$visitingUrl);

        return view('analyse/detail');
    }


    /**
     * 更新分数
     * @return \Illuminate\Http\JsonResponse
     */
    public function snycScore()
    {
        if($this->request->isAjax()) $this->success('',null,self::snycLoadScore(input()));
        $this->success('');
    }

    /**
     * 更新比赛
     * @return \Illuminate\Http\JsonResponse
     */
    public function snycMatch()
    {
        if($this->request->isAjax()) $this->success('',null,\app\common\traits\MatchNotice::snycLoadMatch(input()));
        $this->success('');
    }

    public function notice(){

        $this->title = '比赛分析';
        if( input('title') ) $this->title .= ' - '.input('title');

        $this->initOption();

        $matchUrl =  url('/analyse/teamMatchDetail',['match_time'=>input('match_time'),'team_id'=>input('team_id'),'name'=>input('title').'~近日比赛']);
        $noticeUrl = url('/analyse/matchNotice2',['team_id'=>input('team_id'),'name'=>'比赛预告']);

        $this->view->assign('matchUrl',$matchUrl);
        $this->view->assign('noticeUrl',$noticeUrl);

        return view('analyse/notice');

    }


    public function specialTeam()
    {
        $this->title = '重点关注';


        $this->initOption();

        $specialTeamArr = config('other.special_team');
        $countryArr = config('other.country');

        $this->view->assign('country_array', $countryArr);
        $this->view->assign('special_team', json_encode($specialTeamArr));

        return view('analyse/specialTeam');
    }


}