<?php

namespace app\common\model\sport;

use think\Model;
use traits\model\SoftDelete;


class Club extends Model
{

    use SoftDelete;


    // 表名
    protected $name = 'sport_club';


    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';

    // 定义时间戳字段名
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';
    protected $deleteTime = 'deleted_at';
    protected $dateFormat = 'Y-m-d H:i:s';

    // 追加属性
    protected $append = [
        'type_text',
    ];

    const TYPE1 = 1;
    const TYPE2 = 2;


    public function getTypeList()
    {
        return [
            '1' => __('俱乐部独家FIFA'),
            '2' => __('国家队IM独家PES'),
        ];
    }

    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function country()
    {
        return $this->belongsTo('app\admin\model\Country', 'country_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
