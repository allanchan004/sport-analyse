<?php

namespace app\common\model;

use think\Model;
use traits\model\SoftDelete;


class Country extends Model
{
    use SoftDelete;

    // 表名
    protected $name = 'country';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';

    // 定义时间戳字段名
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';
    protected $deleteTime = 'deleted_at';
    protected $dateFormat = 'Y-m-d H:i:s';

    // 追加属性
    protected $append = [
        'status_text',
        'oceania_name',
    ];
    

    
    public function getStatusList()
    {
        return [
            '1' => __('启用'),
            '0' => __('禁用'),
        ];
    }

    public function getOceaniaTypeList()
    {
        return [
            '1' => __('亚洲'),
            '2' => __('欧洲'),
            '3' => __('非洲'),
            '4' => __('北美洲'),
            '5' => __('南美洲'),
            '6' => __('大洋洲'),
            '7' => __('南极洲'),
        ];
    }

    public function getOceaniaNameAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['oceania_type']) ? $data['oceania_type'] : '');
        $list = $this->getOceaniaTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

}
