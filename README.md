### 后台
```PHP
/ZvmDneOVtU.php/index/login
admin
admin@admin.com
```


### 禁止谷歌
```PHP
User-agent: Googlebot
Disallow: /
```

### 命令
```PHP
php think crud -t country -u 1 --force=true

php think crud -t sport_club -u 1 --imagefield=ico --imagefield=pics --relation=country --relationforeignkey=country_id --force=true
php think crud -t sport_team -u 1 --imagefield=ico --imagefield=pics --relation=sport_club --relationforeignkey=club_id --force=true
php think crud -t sport_score -u 1 --imagefield=ico --imagefield=pics --relation=country --relationforeignkey=country_id --relation=sport_club --relationforeignkey=club_id --force=true
php think crud -t matchnotice -u 1 --imagefield=ico --imagefield=pics --relation=country --relationforeignkey=country_id --relation=sport_club --relationforeignkey=club_id --force=true


```